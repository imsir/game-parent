package com.yunsizhi.service.mongo;


import com.yunsizhi.common.base.mongodb.MongoBaseService;
import com.yunsizhi.common.base.page.PageHelper;
import com.yunsizhi.common.util.StringUtils;
import com.yunsizhi.model.dto.MongoDemoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 物品操作服务类
 *
 * @author Zhongjunlin
 * @date 2020/09/28 10:28
 */
@Service
public class MongoDemoService implements MongoBaseService<MongoDemoDTO> {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void add(MongoDemoDTO bean) {
        mongoTemplate.save(bean);
    }

    @Override
    public void remove(Long id) {
        Query query = new Query(Criteria.where("id").is(id));
        mongoTemplate.remove(query, MongoDemoDTO.class);
    }

    @Override
    public void remove(List<Long> ids) {
        Query query = new Query(Criteria.where("id").in(ids));
        mongoTemplate.remove(query, MongoDemoDTO.class);
    }


    @Override
    public void logicalRemove(Long id) {
        Query query = new Query(Criteria.where("id").is(id));
        Update update = new Update();
        update.set("deleted", true);
        this.mongoTemplate.updateFirst(query, update, MongoDemoDTO.class);
    }

    @Override
    public void logicalRemove(List<Long> ids) {
        Query query = new Query(Criteria.where("id").in(ids));
        Update update = new Update();
        update.set("deleted", true);
        this.mongoTemplate.updateFirst(query, update, MongoDemoDTO.class);
    }

    @Override
    public void update(MongoDemoDTO bean) {
        Query query = new Query(Criteria.where("id").is(bean.getId()));
        Update update = new Update();
        update.set("name", bean.getName());
        update.set("img", bean.getImg());
        if (Objects.nonNull(bean.getDeleted())) {
            update.set("deleted", bean.getDeleted());
        }
        if (Objects.nonNull(bean.getDisable())) {
            update.set("disable", bean.getDisable());
        }
        this.mongoTemplate.updateFirst(query, update, MongoDemoDTO.class);
    }

    @Override
    public MongoDemoDTO findById(Long id) {
        Query query = new Query(Criteria.where("id").is(id).and("deleted").is(false).and("disable").is(false));
        MongoDemoDTO one = this.mongoTemplate.findOne(query, MongoDemoDTO.class);
        return one;
    }

    @Override
    public PageHelper<MongoDemoDTO> findByPage(PageHelper<MongoDemoDTO> page, MongoDemoDTO bean) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and("deleted").is(false).and("disable").is(false);
        if (StringUtils.isNotEmpty(bean.getName())) {
            criteria.and("name").regex(page.getPattern(bean.getName()));
        }
        query.addCriteria(criteria);
        query.with(Sort.by(Sort.Direction.DESC, "creatorTime"));
        page.PageHelper(page.getPageNum(), page.getPageSize(), query);
        List<MongoDemoDTO> list = mongoTemplate.find(query, MongoDemoDTO.class);
        Long count = mongoTemplate.count(query, MongoDemoDTO.class);
        page.PageHelper(count.intValue(), list);
        return page;
    }

    /**
     * 查询所有数据
     *
     * @return
     */
    public List<MongoDemoDTO> findAll() {
        List<MongoDemoDTO> list = mongoTemplate.findAll(MongoDemoDTO.class);
        return list;
    }

}
