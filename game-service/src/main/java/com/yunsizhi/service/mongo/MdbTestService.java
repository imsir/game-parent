package com.yunsizhi.service.mongo;

import com.yunsizhi.model.dto.MdbTest;
import com.yunsizhi.repository.MdbTestRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Zhongjunlin
 * @date 2020/09/28 10:33
 */
@Service
@Slf4j
public class MdbTestService {

    @Autowired
    MdbTestRepository mdbTestRepository;

    public MdbTest getByTel(String tel) {
        if (StringUtils.isBlank(tel)) {
            return null;
        }
        return this.mdbTestRepository.getByTel(tel);
    }


    public MdbTest getById(Long userId) {
        return this.mdbTestRepository.getById(userId);
    }

    public MdbTest save(MdbTest mdbTest) {
        return this.mdbTestRepository.save(mdbTest);
    }
}
