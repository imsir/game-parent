/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : yunsizhi_game

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 26/09/2020 10:29:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '样式属性',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `is_del` int(1) NULL DEFAULT 0 COMMENT '是否删除 0不是 1是',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '性别男', 0);
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '性别女', 0);
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '性别未知', 0);
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', 'radio radio-info radio-inline', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '显示菜单', 0);
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', 'radio radio-danger radio-inline', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '隐藏菜单', 0);
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', 'radio radio-info radio-inline', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', 'radio radio-danger radio-inline', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '停用状态', 0);
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', 'radio radio-info radio-inline', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', 'radio radio-danger radio-inline', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '停用状态', 0);
INSERT INTO `sys_dict_data` VALUES (10, 1, '是', 'Y', 'sys_yes_no', 'radio radio-info radio-inline', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '系统默认是', 0);
INSERT INTO `sys_dict_data` VALUES (11, 2, '否', 'N', 'sys_yes_no', 'radio radio-danger radio-inline', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '系统默认否', 0);
INSERT INTO `sys_dict_data` VALUES (12, 1, '通知', '1', 'sys_notice_type', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '通知', 0);
INSERT INTO `sys_dict_data` VALUES (13, 2, '公告', '2', 'sys_notice_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '公告', 0);
INSERT INTO `sys_dict_data` VALUES (14, 1, '正常', '0', 'sys_notice_status', 'radio radio-info radio-inline', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (15, 2, '关闭', '1', 'sys_notice_status', 'radio radio-danger radio-inline', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '关闭状态', 0);
INSERT INTO `sys_dict_data` VALUES (16, 1, '新增', '1', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (17, 2, '修改', '2', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (18, 3, '保存', '3', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (19, 4, '删除', '4', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (20, 5, '授权', '5', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (21, 6, '导出', '6', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (22, 7, '导入', '7', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (23, 8, '强退', '8', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (24, 9, '禁止访问', '9', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (25, 10, '生成代码', '10', 'sys_oper_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (26, 1, '成功', '0', 'sys_common_status', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (27, 2, '失败', '1', 'sys_common_status', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '停用状态', 0);
INSERT INTO `sys_dict_data` VALUES (28, 1, '娱乐资讯', '0', 't_news_type', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-01-07 14:13:04', '娱乐资讯', 0);
INSERT INTO `sys_dict_data` VALUES (29, 2, '政府资讯', '1', 't_news_type', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-01-07 14:13:00', '政府资讯', 0);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `is_del` int(1) NULL DEFAULT 0 COMMENT '是否删除 0不是 1是',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表', 0);
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表', 0);
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表', 0);
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表', 0);
INSERT INTO `sys_dict_type` VALUES (5, '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表', 0);
INSERT INTO `sys_dict_type` VALUES (6, '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表', 0);
INSERT INTO `sys_dict_type` VALUES (7, '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表', 0);
INSERT INTO `sys_dict_type` VALUES (8, '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表', 0);
INSERT INTO `sys_dict_type` VALUES (9, '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表', 0);
INSERT INTO `sys_dict_type` VALUES (10, '资讯类型', 't_news_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '资讯类型列表', 0);

-- ----------------------------
-- Table structure for sys_loginlog
-- ----------------------------
DROP TABLE IF EXISTS `sys_loginlog`;
CREATE TABLE `sys_loginlog`  (
  `info_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3121 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_loginlog
-- ----------------------------
INSERT INTO `sys_loginlog` VALUES (2879, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 10', '0', '退出成功', '2018-10-18 12:00:49', 0);
INSERT INTO `sys_loginlog` VALUES (2880, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 10', '0', '登录成功', '2018-10-18 12:00:54', 0);
INSERT INTO `sys_loginlog` VALUES (2881, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 10', '0', '登录成功', '2018-10-18 13:29:48', 0);
INSERT INTO `sys_loginlog` VALUES (2882, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 10', '0', '登录成功', '2018-10-18 13:38:37', 0);
INSERT INTO `sys_loginlog` VALUES (2883, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 10', '0', '登录成功', '2018-10-18 13:41:40', 0);
INSERT INTO `sys_loginlog` VALUES (2884, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 10', '0', '登录成功', '2018-10-18 13:44:39', 0);
INSERT INTO `sys_loginlog` VALUES (2885, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '密码输入错误1次，12345678', '2018-10-22 14:38:54', 0);
INSERT INTO `sys_loginlog` VALUES (2886, 'root', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '用户不存在/密码错误', '2018-10-22 14:39:04', 0);
INSERT INTO `sys_loginlog` VALUES (2887, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-10-29 10:10:41', 0);
INSERT INTO `sys_loginlog` VALUES (2888, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-17 15:00:04', 0);
INSERT INTO `sys_loginlog` VALUES (2889, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-17 15:26:34', 0);
INSERT INTO `sys_loginlog` VALUES (2890, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 10:35:55', 0);
INSERT INTO `sys_loginlog` VALUES (2891, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 10:52:59', 0);
INSERT INTO `sys_loginlog` VALUES (2892, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 12:06:35', 0);
INSERT INTO `sys_loginlog` VALUES (2893, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 12:12:37', 0);
INSERT INTO `sys_loginlog` VALUES (2894, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 12:14:37', 0);
INSERT INTO `sys_loginlog` VALUES (2895, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-19 12:14:58', 0);
INSERT INTO `sys_loginlog` VALUES (2896, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 12:15:20', 0);
INSERT INTO `sys_loginlog` VALUES (2897, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 12:17:21', 0);
INSERT INTO `sys_loginlog` VALUES (2898, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 12:20:10', 0);
INSERT INTO `sys_loginlog` VALUES (2899, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 14:13:23', 0);
INSERT INTO `sys_loginlog` VALUES (2900, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 14:25:28', 0);
INSERT INTO `sys_loginlog` VALUES (2901, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 14:29:30', 0);
INSERT INTO `sys_loginlog` VALUES (2902, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 14:32:30', 0);
INSERT INTO `sys_loginlog` VALUES (2903, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 14:42:53', 0);
INSERT INTO `sys_loginlog` VALUES (2904, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 15:07:49', 0);
INSERT INTO `sys_loginlog` VALUES (2905, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 15:32:11', 0);
INSERT INTO `sys_loginlog` VALUES (2906, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 15:35:07', 0);
INSERT INTO `sys_loginlog` VALUES (2907, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 15:37:33', 0);
INSERT INTO `sys_loginlog` VALUES (2908, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 15:50:04', 0);
INSERT INTO `sys_loginlog` VALUES (2909, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 15:58:18', 0);
INSERT INTO `sys_loginlog` VALUES (2910, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 16:18:24', 0);
INSERT INTO `sys_loginlog` VALUES (2911, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 16:22:37', 0);
INSERT INTO `sys_loginlog` VALUES (2912, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 16:26:17', 0);
INSERT INTO `sys_loginlog` VALUES (2913, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 16:33:47', 0);
INSERT INTO `sys_loginlog` VALUES (2914, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 16:36:42', 0);
INSERT INTO `sys_loginlog` VALUES (2915, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 16:54:27', 0);
INSERT INTO `sys_loginlog` VALUES (2916, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 16:58:28', 0);
INSERT INTO `sys_loginlog` VALUES (2917, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 17:19:06', 0);
INSERT INTO `sys_loginlog` VALUES (2918, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 17:27:29', 0);
INSERT INTO `sys_loginlog` VALUES (2919, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 17:44:39', 0);
INSERT INTO `sys_loginlog` VALUES (2920, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 17:48:00', 0);
INSERT INTO `sys_loginlog` VALUES (2921, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 17:50:07', 0);
INSERT INTO `sys_loginlog` VALUES (2922, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-19 17:50:17', 0);
INSERT INTO `sys_loginlog` VALUES (2923, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 17:50:34', 0);
INSERT INTO `sys_loginlog` VALUES (2924, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 17:58:49', 0);
INSERT INTO `sys_loginlog` VALUES (2925, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 18:12:49', 0);
INSERT INTO `sys_loginlog` VALUES (2926, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 18:16:49', 0);
INSERT INTO `sys_loginlog` VALUES (2927, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 18:21:49', 0);
INSERT INTO `sys_loginlog` VALUES (2928, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 18:24:14', 0);
INSERT INTO `sys_loginlog` VALUES (2929, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 18:30:27', 0);
INSERT INTO `sys_loginlog` VALUES (2930, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-19 18:31:40', 0);
INSERT INTO `sys_loginlog` VALUES (2931, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 16:13:46', 0);
INSERT INTO `sys_loginlog` VALUES (2932, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-21 16:25:25', 0);
INSERT INTO `sys_loginlog` VALUES (2933, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 16:25:35', 0);
INSERT INTO `sys_loginlog` VALUES (2934, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-21 16:25:48', 0);
INSERT INTO `sys_loginlog` VALUES (2935, 'admin1', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 16:25:54', 0);
INSERT INTO `sys_loginlog` VALUES (2936, 'admin1', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-21 16:26:19', 0);
INSERT INTO `sys_loginlog` VALUES (2937, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 16:26:20', 0);
INSERT INTO `sys_loginlog` VALUES (2938, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-21 16:26:22', 0);
INSERT INTO `sys_loginlog` VALUES (2939, 'admin1', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 16:26:31', 0);
INSERT INTO `sys_loginlog` VALUES (2940, 'admin1', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-21 16:27:26', 0);
INSERT INTO `sys_loginlog` VALUES (2941, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 16:27:28', 0);
INSERT INTO `sys_loginlog` VALUES (2942, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-21 17:07:16', 0);
INSERT INTO `sys_loginlog` VALUES (2943, 'ptglbmAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 17:07:32', 0);
INSERT INTO `sys_loginlog` VALUES (2944, 'ptglbmAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-21 17:34:25', 0);
INSERT INTO `sys_loginlog` VALUES (2945, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 17:34:26', 0);
INSERT INTO `sys_loginlog` VALUES (2946, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 17:48:28', 0);
INSERT INTO `sys_loginlog` VALUES (2947, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 17:57:25', 0);
INSERT INTO `sys_loginlog` VALUES (2948, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-21 18:02:19', 0);
INSERT INTO `sys_loginlog` VALUES (2949, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 10:34:04', 0);
INSERT INTO `sys_loginlog` VALUES (2950, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 10:44:41', 0);
INSERT INTO `sys_loginlog` VALUES (2951, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 11:26:56', 0);
INSERT INTO `sys_loginlog` VALUES (2952, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 11:30:02', 0);
INSERT INTO `sys_loginlog` VALUES (2953, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 11:34:38', 0);
INSERT INTO `sys_loginlog` VALUES (2954, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 11:41:42', 0);
INSERT INTO `sys_loginlog` VALUES (2955, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 11:49:33', 0);
INSERT INTO `sys_loginlog` VALUES (2956, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 11:53:25', 0);
INSERT INTO `sys_loginlog` VALUES (2957, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 11:57:15', 0);
INSERT INTO `sys_loginlog` VALUES (2958, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 12:00:20', 0);
INSERT INTO `sys_loginlog` VALUES (2959, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 12:05:44', 0);
INSERT INTO `sys_loginlog` VALUES (2960, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 12:10:26', 0);
INSERT INTO `sys_loginlog` VALUES (2961, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 14:03:16', 0);
INSERT INTO `sys_loginlog` VALUES (2962, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 14:26:11', 0);
INSERT INTO `sys_loginlog` VALUES (2963, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 15:12:56', 0);
INSERT INTO `sys_loginlog` VALUES (2964, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 15:19:21', 0);
INSERT INTO `sys_loginlog` VALUES (2965, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 15:27:44', 0);
INSERT INTO `sys_loginlog` VALUES (2966, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 15:32:24', 0);
INSERT INTO `sys_loginlog` VALUES (2967, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 15:54:58', 0);
INSERT INTO `sys_loginlog` VALUES (2968, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 16:59:16', 0);
INSERT INTO `sys_loginlog` VALUES (2969, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 17:37:13', 0);
INSERT INTO `sys_loginlog` VALUES (2970, 'admin', '192.168.0.230', '192.168.0.230', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 17:45:50', 0);
INSERT INTO `sys_loginlog` VALUES (2971, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 17:52:22', 0);
INSERT INTO `sys_loginlog` VALUES (2972, 'admin', '192.168.0.230', '192.168.0.230', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 17:52:25', 0);
INSERT INTO `sys_loginlog` VALUES (2973, 'admin', '192.168.0.230', '192.168.0.230', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-24 17:53:02', 0);
INSERT INTO `sys_loginlog` VALUES (2974, 'ptbmAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '用户不存在/密码错误', '2018-12-24 17:53:14', 0);
INSERT INTO `sys_loginlog` VALUES (2975, 'ptbmAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-24 17:53:27', 0);
INSERT INTO `sys_loginlog` VALUES (2976, 'ptbmAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-24 17:53:33', 0);
INSERT INTO `sys_loginlog` VALUES (2977, 'ptbmAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-24 17:53:39', 0);
INSERT INTO `sys_loginlog` VALUES (2978, 'ptbmAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-24 17:53:44', 0);
INSERT INTO `sys_loginlog` VALUES (2979, 'ptbmAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-24 17:53:54', 0);
INSERT INTO `sys_loginlog` VALUES (2980, 'ptbmAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-24 17:54:00', 0);
INSERT INTO `sys_loginlog` VALUES (2981, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 17:54:12', 0);
INSERT INTO `sys_loginlog` VALUES (2982, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-24 17:55:52', 0);
INSERT INTO `sys_loginlog` VALUES (2983, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-25 09:42:09', 0);
INSERT INTO `sys_loginlog` VALUES (2984, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 09:42:18', 0);
INSERT INTO `sys_loginlog` VALUES (2985, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 09:45:23', 0);
INSERT INTO `sys_loginlog` VALUES (2986, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 10:21:54', 0);
INSERT INTO `sys_loginlog` VALUES (2987, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 10:58:25', 0);
INSERT INTO `sys_loginlog` VALUES (2988, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 13:52:51', 0);
INSERT INTO `sys_loginlog` VALUES (2989, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 13:57:55', 0);
INSERT INTO `sys_loginlog` VALUES (2990, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 13:58:14', 0);
INSERT INTO `sys_loginlog` VALUES (2991, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 13:59:20', 0);
INSERT INTO `sys_loginlog` VALUES (2992, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 13:59:30', 0);
INSERT INTO `sys_loginlog` VALUES (2993, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 14:01:30', 0);
INSERT INTO `sys_loginlog` VALUES (2994, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 14:02:00', 0);
INSERT INTO `sys_loginlog` VALUES (2995, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 14:02:44', 0);
INSERT INTO `sys_loginlog` VALUES (2996, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-25 14:03:01', 0);
INSERT INTO `sys_loginlog` VALUES (2997, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 14:03:16', 0);
INSERT INTO `sys_loginlog` VALUES (2998, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 14:04:31', 0);
INSERT INTO `sys_loginlog` VALUES (2999, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-25 14:04:50', 0);
INSERT INTO `sys_loginlog` VALUES (3000, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '密码输入错误1次，admin123', '2018-12-25 14:05:01', 0);
INSERT INTO `sys_loginlog` VALUES (3001, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-25 14:05:11', 0);
INSERT INTO `sys_loginlog` VALUES (3002, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '密码输入错误2次，admin123', '2018-12-25 14:05:35', 0);
INSERT INTO `sys_loginlog` VALUES (3003, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 14:05:50', 0);
INSERT INTO `sys_loginlog` VALUES (3004, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 14:09:13', 0);
INSERT INTO `sys_loginlog` VALUES (3005, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 14:09:35', 0);
INSERT INTO `sys_loginlog` VALUES (3006, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 14:34:52', 0);
INSERT INTO `sys_loginlog` VALUES (3007, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 14:35:00', 0);
INSERT INTO `sys_loginlog` VALUES (3008, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 14:36:04', 0);
INSERT INTO `sys_loginlog` VALUES (3009, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 14:36:19', 0);
INSERT INTO `sys_loginlog` VALUES (3010, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 15:13:40', 0);
INSERT INTO `sys_loginlog` VALUES (3011, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 15:17:33', 0);
INSERT INTO `sys_loginlog` VALUES (3012, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 15:23:16', 0);
INSERT INTO `sys_loginlog` VALUES (3013, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 15:29:47', 0);
INSERT INTO `sys_loginlog` VALUES (3014, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 15:34:15', 0);
INSERT INTO `sys_loginlog` VALUES (3015, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 16:20:02', 0);
INSERT INTO `sys_loginlog` VALUES (3016, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 16:22:17', 0);
INSERT INTO `sys_loginlog` VALUES (3017, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 16:22:26', 0);
INSERT INTO `sys_loginlog` VALUES (3018, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 16:30:40', 0);
INSERT INTO `sys_loginlog` VALUES (3019, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 16:38:11', 0);
INSERT INTO `sys_loginlog` VALUES (3020, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-25 16:38:13', 0);
INSERT INTO `sys_loginlog` VALUES (3021, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 16:38:27', 0);
INSERT INTO `sys_loginlog` VALUES (3022, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 16:42:45', 0);
INSERT INTO `sys_loginlog` VALUES (3023, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 16:59:33', 0);
INSERT INTO `sys_loginlog` VALUES (3024, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 17:10:10', 0);
INSERT INTO `sys_loginlog` VALUES (3025, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-25 17:20:42', 0);
INSERT INTO `sys_loginlog` VALUES (3026, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 17:20:55', 0);
INSERT INTO `sys_loginlog` VALUES (3027, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 17:23:26', 0);
INSERT INTO `sys_loginlog` VALUES (3028, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 17:27:57', 0);
INSERT INTO `sys_loginlog` VALUES (3029, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 17:30:28', 0);
INSERT INTO `sys_loginlog` VALUES (3030, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 17:35:42', 0);
INSERT INTO `sys_loginlog` VALUES (3031, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 17:38:59', 0);
INSERT INTO `sys_loginlog` VALUES (3032, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 17:42:48', 0);
INSERT INTO `sys_loginlog` VALUES (3033, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 17:51:39', 0);
INSERT INTO `sys_loginlog` VALUES (3034, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 18:07:22', 0);
INSERT INTO `sys_loginlog` VALUES (3035, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 18:11:48', 0);
INSERT INTO `sys_loginlog` VALUES (3036, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 18:21:07', 0);
INSERT INTO `sys_loginlog` VALUES (3037, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-25 18:26:07', 0);
INSERT INTO `sys_loginlog` VALUES (3038, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 09:59:04', 0);
INSERT INTO `sys_loginlog` VALUES (3039, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 10:12:49', 0);
INSERT INTO `sys_loginlog` VALUES (3040, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-26 10:20:48', 0);
INSERT INTO `sys_loginlog` VALUES (3041, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 10:20:57', 0);
INSERT INTO `sys_loginlog` VALUES (3042, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-26 10:21:21', 0);
INSERT INTO `sys_loginlog` VALUES (3043, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-26 10:21:36', 0);
INSERT INTO `sys_loginlog` VALUES (3044, 'deptAdmin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '密码输入错误1次，admin123', '2018-12-26 10:21:46', 0);
INSERT INTO `sys_loginlog` VALUES (3045, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 10:22:20', 0);
INSERT INTO `sys_loginlog` VALUES (3046, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-26 10:24:41', 0);
INSERT INTO `sys_loginlog` VALUES (3047, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 10:24:51', 0);
INSERT INTO `sys_loginlog` VALUES (3048, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 10:39:59', 0);
INSERT INTO `sys_loginlog` VALUES (3049, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 10:45:32', 0);
INSERT INTO `sys_loginlog` VALUES (3050, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 11:01:18', 0);
INSERT INTO `sys_loginlog` VALUES (3051, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 11:11:35', 0);
INSERT INTO `sys_loginlog` VALUES (3052, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 11:22:28', 0);
INSERT INTO `sys_loginlog` VALUES (3053, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-26 11:22:50', 0);
INSERT INTO `sys_loginlog` VALUES (3054, 'admindept2', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '密码输入错误1次，admin123', '2018-12-26 11:23:08', 0);
INSERT INTO `sys_loginlog` VALUES (3055, 'admindept2', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-26 11:23:13', 0);
INSERT INTO `sys_loginlog` VALUES (3056, 'admindept2', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 11:23:23', 0);
INSERT INTO `sys_loginlog` VALUES (3057, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 11:26:16', 0);
INSERT INTO `sys_loginlog` VALUES (3058, 'admin', '192.168.0.184', '192.168.0.184', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 14:15:36', 0);
INSERT INTO `sys_loginlog` VALUES (3059, 'admindept', '192.168.0.95', '192.168.0.95', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 14:16:05', 0);
INSERT INTO `sys_loginlog` VALUES (3060, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 14:39:35', 0);
INSERT INTO `sys_loginlog` VALUES (3061, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 14:51:28', 0);
INSERT INTO `sys_loginlog` VALUES (3062, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 14:55:23', 0);
INSERT INTO `sys_loginlog` VALUES (3063, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 15:00:34', 0);
INSERT INTO `sys_loginlog` VALUES (3064, 'admindept', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-26 16:46:57', 0);
INSERT INTO `sys_loginlog` VALUES (3065, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 10:54:32', 0);
INSERT INTO `sys_loginlog` VALUES (3066, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 10:56:49', 0);
INSERT INTO `sys_loginlog` VALUES (3067, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 11:08:05', 0);
INSERT INTO `sys_loginlog` VALUES (3068, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 11:16:48', 0);
INSERT INTO `sys_loginlog` VALUES (3069, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 11:29:46', 0);
INSERT INTO `sys_loginlog` VALUES (3070, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 13:59:21', 0);
INSERT INTO `sys_loginlog` VALUES (3071, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 14:04:50', 0);
INSERT INTO `sys_loginlog` VALUES (3072, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 14:13:49', 0);
INSERT INTO `sys_loginlog` VALUES (3073, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 14:16:47', 0);
INSERT INTO `sys_loginlog` VALUES (3074, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 14:21:57', 0);
INSERT INTO `sys_loginlog` VALUES (3075, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 14:27:33', 0);
INSERT INTO `sys_loginlog` VALUES (3076, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 14:34:05', 0);
INSERT INTO `sys_loginlog` VALUES (3077, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 14:40:13', 0);
INSERT INTO `sys_loginlog` VALUES (3078, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 14:58:49', 0);
INSERT INTO `sys_loginlog` VALUES (3079, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 15:01:55', 0);
INSERT INTO `sys_loginlog` VALUES (3080, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 15:04:00', 0);
INSERT INTO `sys_loginlog` VALUES (3081, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 15:07:12', 0);
INSERT INTO `sys_loginlog` VALUES (3082, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 15:13:02', 0);
INSERT INTO `sys_loginlog` VALUES (3083, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 15:15:10', 0);
INSERT INTO `sys_loginlog` VALUES (3084, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 15:27:12', 0);
INSERT INTO `sys_loginlog` VALUES (3085, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 15:34:57', 0);
INSERT INTO `sys_loginlog` VALUES (3086, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 15:46:24', 0);
INSERT INTO `sys_loginlog` VALUES (3087, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 15:59:04', 0);
INSERT INTO `sys_loginlog` VALUES (3088, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 16:02:37', 0);
INSERT INTO `sys_loginlog` VALUES (3089, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 16:04:41', 0);
INSERT INTO `sys_loginlog` VALUES (3090, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 16:07:41', 0);
INSERT INTO `sys_loginlog` VALUES (3091, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 16:21:42', 0);
INSERT INTO `sys_loginlog` VALUES (3092, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 16:36:43', 0);
INSERT INTO `sys_loginlog` VALUES (3093, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 16:43:31', 0);
INSERT INTO `sys_loginlog` VALUES (3094, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 16:54:01', 0);
INSERT INTO `sys_loginlog` VALUES (3095, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-27 17:04:46', 0);
INSERT INTO `sys_loginlog` VALUES (3096, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-28 10:15:55', 0);
INSERT INTO `sys_loginlog` VALUES (3097, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-28 10:22:59', 0);
INSERT INTO `sys_loginlog` VALUES (3098, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-28 10:26:42', 0);
INSERT INTO `sys_loginlog` VALUES (3099, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-28 10:32:54', 0);
INSERT INTO `sys_loginlog` VALUES (3100, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2019-01-08 14:22:25', 0);
INSERT INTO `sys_loginlog` VALUES (3101, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-08 14:22:30', 0);
INSERT INTO `sys_loginlog` VALUES (3102, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-08 15:48:31', 0);
INSERT INTO `sys_loginlog` VALUES (3103, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '退出成功', '2019-01-08 15:54:05', 0);
INSERT INTO `sys_loginlog` VALUES (3104, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-08 15:54:13', 0);
INSERT INTO `sys_loginlog` VALUES (3105, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-10 10:20:42', 0);
INSERT INTO `sys_loginlog` VALUES (3106, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-10 14:51:34', 0);
INSERT INTO `sys_loginlog` VALUES (3107, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-10 15:05:41', 0);
INSERT INTO `sys_loginlog` VALUES (3108, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 15:07:10', 0);
INSERT INTO `sys_loginlog` VALUES (3109, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 15:17:00', 0);
INSERT INTO `sys_loginlog` VALUES (3110, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 16:50:02', 0);
INSERT INTO `sys_loginlog` VALUES (3111, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 16:57:10', 0);
INSERT INTO `sys_loginlog` VALUES (3112, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 17:20:08', 0);
INSERT INTO `sys_loginlog` VALUES (3113, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 17:22:17', 0);
INSERT INTO `sys_loginlog` VALUES (3114, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 17:25:29', 0);
INSERT INTO `sys_loginlog` VALUES (3115, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 17:32:43', 0);
INSERT INTO `sys_loginlog` VALUES (3116, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 17:39:31', 0);
INSERT INTO `sys_loginlog` VALUES (3117, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 18:00:48', 0);
INSERT INTO `sys_loginlog` VALUES (3118, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 18:11:15', 0);
INSERT INTO `sys_loginlog` VALUES (3119, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 18:19:05', 0);
INSERT INTO `sys_loginlog` VALUES (3120, 'admin', '127.0.0.1', '127.0.0.1', 'Chrome', 'Windows 7', '0', '登录成功', '2019-01-15 18:28:09', 0);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT NULL COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求地址',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单图标',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `status` int(10) NULL DEFAULT NULL COMMENT '状态描述字段',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除状态，0=未删除 1=已删除',
  `version` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '版本号',
  `creator` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `creator_id` int(11) NULL DEFAULT NULL COMMENT '创建人id',
  `creator_time` datetime(0) NOT NULL COMMENT '创建时间',
  `last_operator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '最近操作人',
  `last_operator_id` int(11) NULL DEFAULT NULL COMMENT '最近操作人id',
  `last_operator_time` datetime(0) NULL DEFAULT NULL COMMENT '最近操作时间',
  `last_operator_source` int(10) NULL DEFAULT NULL COMMENT '最近操作来源 1APP 2PC 3 PDA',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1110 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, '#', 'M', '0', '', 'fa fa-gear', '系统管理目录', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, '#', 'M', '0', '', 'fa fa-video-camera', '系统监控目录', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (3, '数据目录管理', 0, 3, '#', 'M', '0', '', 'fa fa-search', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (4, '文件管理', 0, 4, '#', 'M', '0', '', 'fa fa-archive', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5, '资讯管理', 0, 5, '#', 'M', '0', '', 'fa fa-comment', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6, '应用管理', 0, 6, '#', 'M', '0', '', 'fa fa-archive', '应用管理', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', 'C', '0', 'system:user:view', '#', '用户管理菜单', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', 'C', '0', 'system:role:view', '#', '角色管理菜单', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', 'C', '0', 'system:dict:view', '#', '字典管理菜单', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (107, '数据监控', 2, 1, '/monitor/data', 'C', '0', 'monitor:data:view', '', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', 'M', '0', '', '#', '日志管理菜单', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', 'C', '0', 'monitor:online:view', '#', '在线用户菜单', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', 'C', '0', 'monitor:operlog:view', '#', '操作日志菜单', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', 'C', '0', 'monitor:logininfor:view', '#', '登录日志菜单', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', 'F', '0', 'system:user:list', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', 'F', '0', 'system:user:add', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', 'F', '0', 'system:user:edit', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', 'F', '0', 'system:user:remove', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1004, '用户保存', 100, 5, '#', 'F', '0', 'system:user:save', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1005, '重置密码', 100, 6, '#', 'F', '0', 'system:user:resetPwd', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1006, '角色查询', 101, 1, '#', 'F', '0', 'system:role:list', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1007, '角色新增', 101, 2, '#', 'F', '0', 'system:role:add', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1008, '角色修改', 101, 3, '#', 'F', '0', 'system:role:edit', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1009, '角色删除', 101, 4, '#', 'F', '0', 'system:role:remove', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1010, '角色保存', 101, 5, '#', 'F', '0', 'system:role:save', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', 'F', '0', 'system:dict:list', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', 'F', '0', 'system:dict:add', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', 'F', '0', 'system:dict:edit', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', 'F', '0', 'system:dict:remove', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1030, '字典保存', 105, 5, '#', 'F', '0', 'system:dict:save', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1041, '操作查询', 500, 1, '#', 'F', '0', 'monitor:operlog:list', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1042, '操作删除', 500, 2, '#', 'F', '0', 'monitor:operlog:remove', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1043, '详细信息', 500, 3, '#', 'F', '0', 'monitor:operlog:detail', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1044, '登录查询', 501, 1, '#', 'F', '0', 'monitor:logininfor:list', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1045, '登录删除', 501, 2, '#', 'F', '0', 'monitor:logininfor:remove', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', 'F', '0', 'monitor:online:list', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', 'F', '0', 'monitor:online:batchForceLogout', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', 'F', '0', 'monitor:online:forceLogout', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1049, '部门管理', 1, 3, '/system/dept', 'C', '0', 'system:dept:view', '#', '部门管理', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1050, '部门新增', 1049, 1, '#', 'F', '0', 'system:dept:add', '#', '部门新增', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1051, '部门查询', 1049, 2, '#', 'F', '0', 'system:dept:list', '#', '部门查询', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1052, '部门修改', 1049, 3, '#', 'F', '0', 'system:dept:edit', '#', '部门修改', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1053, '部门删除', 1049, 4, '#', 'F', '0', 'system:dept:remove', '#', '部门删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1054, '部门保存', 1049, 5, '#', 'F', '0', 'system:dept:save', '#', '部门保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1055, '子账号管理', 1, 4, '/system/childUser', 'C', '0', 'system:childUser:view', '#', '子账号管理', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1056, '子账号添加', 1055, 1, '#', 'F', '0', 'system:childUser:add', '#', '子账号添加', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1057, '子账号列表', 1055, 2, '#', 'F', '0', 'system:childUser:list', '#', '子账号列表', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1058, '子账号编辑', 1055, 3, '#', 'F', '0', 'system:childUser:edit', '#', '子账号编辑', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1059, '子账号删除', 1055, 4, '#', 'F', '0', 'system:childUser:remove', '#', '子账号删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1060, '子账号保存', 1055, 5, '#', 'F', '0', 'system:childUser:save', '#', '子账号保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1061, '子账号重置密码', 1055, 6, '#', 'F', '0', 'system:childUser:resetPwd', '#', '子账号重置密码', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1062, '数据目录', 3, 5, '/data/directory', 'C', '0', 'data:directory:view', '#', '数据目录', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1063, '数据目录列表查询', 1062, 1, '#', 'F', '0', 'data:directory:list', '#', '数据目录列表查询', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1064, '数据目录查询One', 1062, 2, '#', 'F', '0', 'data:directory:selectOne', '#', '数据目录查询One', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1065, '数据目录保存', 1062, 3, '#', 'F', '0', 'data:directory:save', '#', '数据目录保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1066, '数据目录删除', 1062, 4, '#', 'F', '0', 'data:directory:remove', '#', '数据目录删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1067, '数据项', 3, 6, '/data/item', 'C', '0', 'data:item:view', '#', '数据项', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1068, '数据项查询列表', 1067, 1, '#', 'F', '0', 'data:item:list', '#', '数据项查询列表', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1069, '数据项目录查询', 1067, 2, '#', 'F', '0', 'data:item:selectOne', '#', '数据项目录查询', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1070, '数据项目录保存', 1067, 3, '#', 'F', '0', 'data:item:save', '#', '数据项目录保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1071, '数据目录删除', 1067, 4, '#', 'F', '0', 'data:item:remove', '#', '数据目录删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1072, '数据目录文件下载', 3, 7, '/data/directDownload', 'C', '0', 'data:directDownload:view', '#', '数据目录文件下载', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1073, '数据目录文件下载查询列表', 1072, 1, '#', 'F', '0', 'data:directDownload:list', '#', '数据目录文件下载查询列表', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1074, '数据目录文件下载目录查询', 1072, 2, '#', 'F', '0', 'data:directDownload:selectOne', '#', '数据目录文件下载目录查询', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1075, '数据目录文件下载目录保存', 1072, 3, '#', 'F', '0', 'data:directDownload:save', '#', '数据目录文件下载目录保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1076, '数据目录删除', 1072, 4, '#', 'F', '0', 'data:directDownload:remove', '#', '数据目录删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1077, 'API评论交流', 3, 8, '/data/directCommen', 'C', '0', 'data:directCommen:view', '#', 'API评论交流', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1078, 'API评论交流查询列表', 1077, 1, '#', 'F', '0', 'data:directCommen:list', '#', 'API评论交流查询列表', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1079, 'API评论交流目录查询', 1077, 2, '#', 'F', '0', 'data:directCommen:selectOne', '#', 'API评论交流目录查询', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1080, 'API评论交流目录保存', 1077, 3, '#', 'F', '0', 'data:directCommen:save', '#', 'API评论交流目录保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1081, '数据目录删除', 1077, 4, '#', 'F', '0', 'data:directCommen:remove', '#', '数据目录删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1082, 'api服务', 3, 9, '/api/server', 'C', '0', 'api:server:view', '#', 'api服务', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1083, 'API服务查询列表', 1082, 1, '#', 'F', '0', 'api:server:list', '#', 'API服务查询列表', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1084, 'API服务目录查询', 1082, 2, '#', 'F', '0', 'api:server:selectOne', '#', 'API服务目录查询', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1085, 'API服务目录保存', 1082, 3, '#', 'F', '0', 'api:server:save', '#', 'API服务目录保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1086, 'API服务删除', 1082, 4, '#', 'F', '0', 'api:server:remove', '#', 'API服务删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1087, '文件管理', 4, 10, '/file/management', 'C', '0', 'file:management:view', '#', '文件管理', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1088, '文件管理查询列表', 1087, 1, '#', 'F', '0', 'file:management:list', '#', '文件管理查询列表', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1089, '文件管理查询', 1087, 2, '#', 'F', '0', 'file:management:selectOne', '#', '文件管理查询', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1090, '文件管理保存', 1087, 3, '#', 'F', '0', 'file:management:save', '#', '文件管理保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1091, '文件管理删除', 1087, 4, '#', 'F', '0', 'file:management:remove', '#', '文件管理删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1092, '文件类型', 4, 11, '/file/type', 'C', '0', 'file:type:view', '#', '文件类型', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1093, '文件类型查询列表', 1092, 1, '#', 'F', '0', 'file:type:list', '#', '文件类型查询列表', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1094, '文件类型查询', 1092, 2, '#', 'F', '0', 'file:type:selectOne', '#', '文件类型查询', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1095, '文件类型保存', 1092, 3, '#', 'F', '0', 'file:type:save', '#', '文件类型保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1096, '文件类型删除', 1092, 4, '#', 'F', '0', 'file:type:remove', '#', '文件类型删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1097, '文件上传', 4, 12, '/file', 'C', '0', 'file:view', '#', '文件上传', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1098, '上传', 1097, 1, '#', 'F', '0', 'file:upload', '#', '上传', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1099, '资讯管理', 5, 13, '/news', 'C', '0', 'new:view', '#', '咨询管理', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1100, '资讯查询列表', 1099, 1, '#', 'F', '0', 'news:list', '#', '资讯查询列表', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1101, '资讯查询', 1099, 2, '#', 'F', '0', 'news:selectOne', '#', '资讯查询', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1102, '资讯保存', 1099, 3, '#', 'F', '0', 'news:save', '#', '资讯保存', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1103, '资讯删除', 1099, 4, '#', 'F', '0', 'news:remove', '#', '资讯删除', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1104, '应用管理', 6, 14, '/application/list', 'C', '0', 'app:application:view', '#', '应用管理', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1105, '应用列表', 1104, 1, '#', 'F', '0', 'app:application:list', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1106, '应用编辑', 1104, 2, '#', 'F', '0', 'app:application:edit', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1107, '应用KEY重置', 1104, 3, '#', 'F', '0', 'app:application:resetkey', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1108, '应用保存', 1104, 4, '#', 'F', '0', 'app:application:save', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1109, '应用删除', 1104, 5, '#', 'F', '0', 'api:user:getUserInfo', '#', '', NULL, 0, NULL, NULL, NULL, '2020-09-24 16:29:31', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `status` int(10) NULL DEFAULT NULL COMMENT '状态描述字段',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除状态，0=未删除 1=已删除',
  `version` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '版本号',
  `creator` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `creator_id` int(11) NULL DEFAULT NULL COMMENT '创建人id',
  `creator_time` datetime(0) NOT NULL COMMENT '创建时间',
  `last_operator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '最近操作人',
  `last_operator_id` int(11) NULL DEFAULT NULL COMMENT '最近操作人id',
  `last_operator_time` datetime(0) NULL DEFAULT NULL COMMENT '最近操作时间',
  `last_operator_source` int(10) NULL DEFAULT NULL COMMENT '最近操作来源 1APP 2PC 3 PDA',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '管理员', NULL, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `sys_role` VALUES (3, '平台管理角色', 'pingtaiAdmin', 2, '', NULL, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `sys_role` VALUES (4, '平台开发部门', 'kaifa', 3, '', NULL, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `sys_role` VALUES (5, '部门管理员', 'deptAdmin', 4, '', NULL, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);
INSERT INTO `sys_role` VALUES (6, '普通用户', 'ordinaryUser', 6, '', NULL, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1, 0);
INSERT INTO `sys_role_menu` VALUES (1, 2, 0);
INSERT INTO `sys_role_menu` VALUES (1, 3, 0);
INSERT INTO `sys_role_menu` VALUES (1, 4, 0);
INSERT INTO `sys_role_menu` VALUES (1, 5, 0);
INSERT INTO `sys_role_menu` VALUES (1, 6, 0);
INSERT INTO `sys_role_menu` VALUES (1, 100, 0);
INSERT INTO `sys_role_menu` VALUES (1, 101, 0);
INSERT INTO `sys_role_menu` VALUES (1, 105, 0);
INSERT INTO `sys_role_menu` VALUES (1, 107, 0);
INSERT INTO `sys_role_menu` VALUES (1, 108, 0);
INSERT INTO `sys_role_menu` VALUES (1, 109, 0);
INSERT INTO `sys_role_menu` VALUES (1, 500, 0);
INSERT INTO `sys_role_menu` VALUES (1, 501, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1000, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1001, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1002, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1003, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1004, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1005, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1006, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1007, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1008, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1009, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1010, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1026, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1027, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1028, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1029, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1030, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1041, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1042, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1043, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1044, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1045, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1046, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1047, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1048, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1049, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1050, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1051, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1052, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1053, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1054, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1055, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1056, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1057, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1058, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1059, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1060, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1061, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1062, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1063, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1064, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1065, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1066, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1067, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1068, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1069, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1070, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1071, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1072, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1073, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1074, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1075, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1076, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1077, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1078, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1079, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1080, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1081, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1082, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1083, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1084, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1085, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1086, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1087, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1088, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1089, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1090, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1091, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1092, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1093, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1094, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1095, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1096, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1097, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1098, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1099, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1100, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1101, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1102, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1103, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1104, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1105, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1106, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1107, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1108, 0);
INSERT INTO `sys_role_menu` VALUES (1, 1109, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2, 0);
INSERT INTO `sys_role_menu` VALUES (2, 107, 0);
INSERT INTO `sys_role_menu` VALUES (2, 109, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1046, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1047, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1048, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1049, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1051, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1, 0);
INSERT INTO `sys_role_menu` VALUES (3, 100, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1000, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1001, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1002, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1003, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1004, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1005, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1049, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1050, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1051, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1052, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1053, 0);
INSERT INTO `sys_role_menu` VALUES (3, 1054, 0);
INSERT INTO `sys_role_menu` VALUES (4, 1, 0);
INSERT INTO `sys_role_menu` VALUES (4, 2, 0);
INSERT INTO `sys_role_menu` VALUES (4, 107, 0);
INSERT INTO `sys_role_menu` VALUES (4, 108, 0);
INSERT INTO `sys_role_menu` VALUES (4, 109, 0);
INSERT INTO `sys_role_menu` VALUES (4, 500, 0);
INSERT INTO `sys_role_menu` VALUES (4, 501, 0);
INSERT INTO `sys_role_menu` VALUES (4, 1041, 0);
INSERT INTO `sys_role_menu` VALUES (4, 1042, 0);
INSERT INTO `sys_role_menu` VALUES (4, 1043, 0);
INSERT INTO `sys_role_menu` VALUES (4, 1044, 0);
INSERT INTO `sys_role_menu` VALUES (4, 1045, 0);
INSERT INTO `sys_role_menu` VALUES (4, 1046, 0);
INSERT INTO `sys_role_menu` VALUES (4, 1047, 0);
INSERT INTO `sys_role_menu` VALUES (4, 1048, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1, 0);
INSERT INTO `sys_role_menu` VALUES (5, 100, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1000, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1001, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1002, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1003, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1004, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1005, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1049, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1050, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1051, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1052, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1053, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1054, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1055, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1056, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1057, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1058, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1059, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1060, 0);
INSERT INTO `sys_role_menu` VALUES (5, 1061, 0);
INSERT INTO `sys_role_menu` VALUES (6, 1, 0);
INSERT INTO `sys_role_menu` VALUES (6, 2, 0);
INSERT INTO `sys_role_menu` VALUES (6, 100, 0);
INSERT INTO `sys_role_menu` VALUES (6, 107, 0);
INSERT INTO `sys_role_menu` VALUES (6, 108, 0);
INSERT INTO `sys_role_menu` VALUES (6, 109, 0);
INSERT INTO `sys_role_menu` VALUES (6, 500, 0);
INSERT INTO `sys_role_menu` VALUES (6, 1000, 0);
INSERT INTO `sys_role_menu` VALUES (6, 1041, 0);
INSERT INTO `sys_role_menu` VALUES (6, 1043, 0);
INSERT INTO `sys_role_menu` VALUES (6, 1046, 0);
INSERT INTO `sys_role_menu` VALUES (6, 1049, 0);
INSERT INTO `sys_role_menu` VALUES (6, 1051, 0);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像路径',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '盐加密',
  `login_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `status` int(10) NULL DEFAULT NULL COMMENT '状态描述字段',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除状态，0=未删除 1=已删除',
  `version` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '版本号',
  `creator` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `creator_id` int(11) NULL DEFAULT NULL COMMENT '创建人id',
  `creator_time` datetime(0) NOT NULL COMMENT '创建时间',
  `last_operator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '最近操作人',
  `last_operator_id` int(11) NULL DEFAULT NULL COMMENT '最近操作人id',
  `last_operator_time` datetime(0) NULL DEFAULT NULL COMMENT '最近操作时间',
  `last_operator_source` int(10) NULL DEFAULT NULL COMMENT '最近操作来源 1APP 2PC 3 PDA',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '管理员', '00', '12eqw@adsf.com', '18349278737', '0', '', '184600fe0d788800fec2e8505cfd15c78b275b94585daaa9ae7b709ce5b304fe', '72d65d', '127.0.0.1', '2019-01-15 18:28:09', '管理员', 0, 0, NULL, NULL, NULL, '2020-09-24 14:08:25', NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (2, 'test', '管理员', '00', '12eqw@adsf.com', '18300001111', '0', '', '184600fe0d788800fec2e8505cfd15c78b275b94585daaa9ae7b709ce5b304fe', '72d65d', '127.0.0.1', '2019-01-15 18:28:09', '管理员', 0, 0, NULL, NULL, NULL, '2020-09-24 14:08:25', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 0);
INSERT INTO `sys_user_role` VALUES (2, 1, 0);

SET FOREIGN_KEY_CHECKS = 1;
