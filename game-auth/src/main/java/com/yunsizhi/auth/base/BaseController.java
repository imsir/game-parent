package com.yunsizhi.auth.base;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yunsizhi.auth.shiro.service.TokenService;
import com.yunsizhi.auth.shiro.token.LoginUser;
import com.yunsizhi.common.base.page.PageDomain;
import com.yunsizhi.common.base.page.TableDataInfo;
import com.yunsizhi.common.base.page.TableSupport;
import com.yunsizhi.common.constants.Constants;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.common.util.ServletUtils;
import com.yunsizhi.common.util.StringUtils;
import com.yunsizhi.model.dto.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * web层通用数据处理
 *
 * @author zjl
 */
public class BaseController {

    @Autowired
    private TokenService tokenService;

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        String theAlias = pageDomain.getTheAlias();
        String orderBy = null;

        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            orderBy = pageDomain.getOrderBy();
        } else {
            pageNum = 1;
            pageSize = 10;
        }

        if (StringUtils.isNotEmpty(theAlias) && StringUtils.isNotEmpty(pageDomain.getOrderByColumn())) {
            orderBy = theAlias + "." + pageDomain.getOrderBy();
        }

        PageHelper.startPage(pageNum, pageSize, orderBy);
    }

    /**
     * Mongodb分页参数初始化
     *
     * @return
     */
    protected com.yunsizhi.common.base.page.PageHelper initPage() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        if (Objects.isNull(pageNum) || pageNum <= 0) {
            pageNum = 1;
        }
        Integer pageSize = pageDomain.getPageSize();
        if (Objects.isNull(pageSize) || pageSize <= 0) {
            pageSize = 10;
        }
        com.yunsizhi.common.base.page.PageHelper pageHelper = new com.yunsizhi.common.base.page.PageHelper(pageNum, pageSize);
        return pageHelper;
    }

    /**
     * 响应请求分页数据
     */
    protected TableDataInfo getDataTable(List<?> list) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 设置请求分页数据
     */
    protected void startAppPage() {

        Integer pageNum = ServletUtils.getParameterToInt(Constants.PAGENUM);
        Integer pageSize = ServletUtils.getParameterToInt(Constants.PAGESIZE);
        String orderByColumn = ServletUtils.getParameter(Constants.ORDERBYCOLUMN);
        Integer isAsc = ServletUtils.getParameterToInt(Constants.ISASC);
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            String orderBy = "";
            if (StringUtils.isNotNull(orderByColumn)) {
                // 否
                if (StringUtils.isNotNull(isAsc) && isAsc == 0) {
                    orderBy = StringUtils.toUnderScoreCase(orderByColumn);
                    // 是
                } else if (StringUtils.isNotNull(isAsc) && isAsc == 1) {
                    orderBy = StringUtils.toUnderScoreCase(orderByColumn) + " " + "DESC";
                } else {// 默认
                    orderBy = StringUtils.toUnderScoreCase(orderByColumn) + " " + "DESC";
                }
            }
            PageHelper.startPage(pageNum, pageSize, orderBy);
        }
    }

    /**
     * 返回成功
     */
    public JsonResult success() {
        return JsonResult.success();
    }

    /**
     * 返回成功
     */
    public JsonResult success(Object data) {
        JsonResult JsonResult = new JsonResult();
        JsonResult.put("code", "200");
        JsonResult.put("msg", "操作成功");
        JsonResult.put("data", data);
        return JsonResult;
    }

    /**
     * 返回失败消息
     */
    public JsonResult error() {
        return JsonResult.error();
    }

    /**
     * 返回失败消息
     */
    public JsonResult error(Map<String, String> map) {
        return JsonResult.error(map);
    }

    /**
     * 返回成功消息
     */
    public JsonResult success(String message) {
        return JsonResult.success(message);
    }

    /**
     * 返回失败消息
     */
    public JsonResult error(String message) {
        return JsonResult.error(message);
    }

    /**
     * 返回错误码消息
     */
    public JsonResult error(Integer code, String message) {
        return JsonResult.error(code, message);
    }

    /**
     * 页面跳转
     */
    public String redirect(String url) {
        return StringUtils.format("redirect:{}", url);
    }

    public SysUser getUser() {
        LoginUser loginUser = tokenService.getLoginUser();
        return loginUser.getUser();
    }


    public Long getUserId() {
        return getUser().getId();
    }

    public String getLoginName() {
        return getUser().getLoginName();
    }
}
