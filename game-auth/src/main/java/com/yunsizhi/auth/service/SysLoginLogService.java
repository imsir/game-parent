package com.yunsizhi.auth.service;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunsizhi.mapper.system.SysLoginLogMapper;
import com.yunsizhi.model.dto.SysLoginLog;
import org.springframework.stereotype.Service;

/**
 * 系统登录日志记录
 *
 * @author zjl
 */
@Service
public class SysLoginLogService extends ServiceImpl<SysLoginLogMapper, SysLoginLog> {

}
