package com.yunsizhi.auth.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunsizhi.mapper.system.SysOperLogMapper;
import com.yunsizhi.model.dto.SysOperLog;
import org.springframework.stereotype.Service;

/**
 * 操作日志 服务层处理
 *
 * @author zjl
 */
@Service
public class SysOperLogService extends ServiceImpl<SysOperLogMapper, SysOperLog> {
}
