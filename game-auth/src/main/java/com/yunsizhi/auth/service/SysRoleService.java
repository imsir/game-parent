package com.yunsizhi.auth.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunsizhi.common.support.Convert;
import com.yunsizhi.common.util.StringUtils;
import com.yunsizhi.mapper.system.SysRoleMapper;
import com.yunsizhi.mapper.system.SysRoleMenuMapper;
import com.yunsizhi.mapper.system.SysUserRoleMapper;
import com.yunsizhi.model.dto.SysRole;
import com.yunsizhi.model.dto.SysRoleMenu;
import com.yunsizhi.model.dto.SysUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 角色 业务层处理
 *
 * @author zjl
 */
@Service
public class SysRoleService extends ServiceImpl<SysRoleMapper, SysRole> {
    @Autowired
    SysRoleMenuMapper roleMenuMapper;
    @Autowired
    SysUserRoleMapper userRoleMapper;

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    public Set<String> selectRoleKeys(Long userId) {
        List<SysRole> perms = this.baseMapper.selectRolesByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRole perm : perms) {
            if (StringUtils.isNotNull(perms)) {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim()
                        .split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    public int saveRole(SysRole role) {

        Long roleId = role.getId();
        if (StringUtils.isNotNull(roleId)) {

           // role.setLastOperator(ShiroUtils.getLoginName());
            //role.setLastOperatorTime(new Date());
            //role.setLastOperatorId(ShiroUtils.getUserId());

            // 修改角色信息
            this.baseMapper.updateById(role);

            // 删除角色与菜单关联
            this.roleMenuMapper.delete(new QueryWrapper<SysRoleMenu>().eq("role_id", roleId));

        } else {
            //role.setCreator(ShiroUtils.getLoginName());
            //role.setCreatorTime(new Date());
            //role.setCreatorId(ShiroUtils.getUserId());
            // 新增角色信息
            this.baseMapper.insert(role);
        }
        return insertRoleMenu(role);
    }

    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public int insertRoleMenu(SysRole role) {
        int rows = 1;
        if (StringUtils.isNotNulls(role.getMenuIds())) {
            // 新增用户与角色管理
            List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
            for (Long menuId : role.getMenuIds()) {
                SysRoleMenu rm = new SysRoleMenu();
                rm.setRoleId(role.getId());
                rm.setMenuId(menuId);
                list.add(rm);
            }
            if (list.size() > 0) {
                rows = roleMenuMapper.batchRoleMenu(list);
            }
        }
        return rows;
    }

    /**
     * 批量删除角色信息
     *
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    public void deleteRoleByIds(String ids) throws Exception {
        Long[] roleIds = Convert.toLongArray(ids);

        List<Long> idList = Arrays.asList(roleIds);
        //逻辑删除
        SysRole r = new SysRole();
        r.setDeleted(1);
        for (Long id : idList) {
            r.setId(id);
            this.baseMapper.updateById(r);
        }
        //物理删除
		/*
		for (Long roleId : roleIds) {
			SysRole role = this.baseMapper.selectById(roleId);
			if (countUserRoleByRoleId(roleId) > 0) {
				throw new Exception(String.format("%1$s已分配,不能删除",role.getRoleName()));
			}
			// 删除角色与菜单关联
			this.roleMenuMapper.delete(new EntityWrapper<SysRoleMenu>().eq("role_id", roleId));
		}
		this.baseMapper.deleteBatchIds(idList);
		*/
    }

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public int countUserRoleByRoleId(Long roleId) {

        return userRoleMapper.selectCount(new QueryWrapper<SysUserRole>().eq(
                "role_id", roleId));
    }

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<SysRole> selectRolesByUserId(Long userId) {

        List<SysRole> userRoles = this.baseMapper.selectRolesByUserId(userId);
        List<SysRole> roles = this.baseMapper.selectList(null);
        for (SysRole role : roles) {
            if (userRoles != null && userRoles.size() != 0) {
                for (SysRole userRole : userRoles) {
                    if (StringUtils.isNotNull(userRole) && userRole.getId() != null) {
                        if (Objects.equals(role.getId(), userRole.getId())) {
                            role.setFlag(true);
                            break;
                        }
                    }
                }
            }
        }
        return roles;
    }

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<SysRole> selectRolesListByUserId(Long userId) {
        return this.baseMapper.selectRolesByUserId(userId);
    }

    public List<SysRole> searchList(Map<String, Object> columnMap) {
        return this.baseMapper.searchList(columnMap);
    }
}
