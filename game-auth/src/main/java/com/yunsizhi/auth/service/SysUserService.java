package com.yunsizhi.auth.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunsizhi.auth.shiro.util.CommonsUtils;
import com.yunsizhi.common.support.Convert;
import com.yunsizhi.common.util.StringUtils;
import com.yunsizhi.mapper.system.SysRoleMapper;
import com.yunsizhi.mapper.system.SysUserMapper;
import com.yunsizhi.mapper.system.SysUserRoleMapper;
import com.yunsizhi.model.dto.SysRole;
import com.yunsizhi.model.dto.SysUser;
import com.yunsizhi.model.dto.SysUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 用户 业务层处理
 *
 * @author zjl
 */
@Service
public class SysUserService extends ServiceImpl<SysUserMapper, SysUser> {

    @Autowired
    private SysRoleMapper roleMapper;
    @Autowired
    private SysUserRoleMapper userRoleMapper;


    /**
     * 查询用户列表
     *
     * @param user
     * @return
     */
    public List<SysUser> selectUserList(SysUser user) {
        return this.baseMapper.selectUserList(user);
    }

    /**
     * 根据用户ID查询当前用户所拥有的角色列表
     *
     * @param userId 用户ID
     * @return 结果
     */
    public List<SysRole> selectUserRoleList(Long userId) {

        return this.roleMapper.selectRolesByUserId(userId);
    }

    /**
     * 查询用户所属角色组
     *
     * @param userId 用户ID
     * @return 结果
     */
    public String selectUserRoleGroup(Long userId) {

        List<SysRole> list = this.roleMapper.selectRolesByUserId(userId);
        StringBuffer idsStr = new StringBuffer();
        for (SysRole role : list) {
            idsStr.append(role.getRoleName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 查询用户所属角色
     *
     * @param userId 用户ID
     * @return 结果
     */
    public SysRole selectRoleByUserId(Long userId) {
        List<SysRole> list = this.roleMapper.selectRolesByUserId(userId);
        return list.get(0);
    }


    /**
     * 修改用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    public int resetUserPwd(SysUser user) {
        user.setPassword(CommonsUtils.encryptPassword(user.getPassword(), user.getSalt()));
        return this.baseMapper.updateById(user);
    }

    /**
     * 批量删除用户信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public void deleteUserByIds(String ids) throws Exception {
        Long[] userIds = Convert.toLongArray(ids);
        List<Long> idsList = Arrays.asList(userIds);

        //以下逻辑删除
        SysUser u = new SysUser();
        u.setDeleted(1);
        for (Long id : idsList) {
            u.setId(id);
            this.baseMapper.updateById(u);
        }
        //以下物理删除
        /*
		this.baseMapper.deleteBatchIds(idsList);
		for (Long userId : userIds) {
			if (userId != null && userId == ShiroUtils.getUser().getUserId()) {
				throw new Exception("不允许删除自己");
			}
			if (userId != null && 1L == userId) {
				throw new Exception("不允许删除超级管理员用户");
			} else {
				// 删除用户与角色关联
				userRoleMapper.delete(new EntityWrapper<SysUserRole>().eq("user_id", userId));
			}

		}
		*/
    }

    /**
     * 保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    public int saveUser(SysUser user) {
        int count = 0;
        Long userId = user.getId();
        if (StringUtils.isNotNull(userId)) {
            //user.setLastOperator(ShiroUtils.getLoginName());
            user.setLoginDate(new Date());
            // 修改用户信息
            count = this.baseMapper.updateById(user);
            // 删除用户与角色关联
            userRoleMapper.delete(new QueryWrapper<SysUserRole>().eq("user_id", userId));

            // 新增用户与角色管理
            insertUserRole(user);
        } else {
            user.setPassword(CommonsUtils.encryptPassword(user.getPassword(), user.getSalt()));
            //user.setCreator(ShiroUtils.getLoginName());
            user.setCreatorTime(new Date());
            //user.setCreatorId(ShiroUtils.getUserId());
            // 新增用户信息
            count = this.baseMapper.insert(user);
            // 新增用户与角色管理
            insertUserRole(user);
        }
        return count;
    }

    /**
     * 新增用户角色信息
     * 空格
     * 呵呵
     *
     * @param user 用户对象
     */
    public void insertUserRole(SysUser user) {
        if (StringUtils.isNotNulls(user.getRoleIds())) {
            // 新增用户与角色管理
            List<SysUserRole> list = new ArrayList<SysUserRole>();
            for (Long roleId : user.getRoleIds()) {
                SysUserRole ur = new SysUserRole();
                ur.setUserId(user.getId());
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0) {
                userRoleMapper.batchUserRole(list);
            }
        }
    }

}
