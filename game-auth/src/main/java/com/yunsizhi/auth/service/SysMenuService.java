package com.yunsizhi.auth.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunsizhi.auth.shiro.util.TreeUtils;
import com.yunsizhi.common.util.StringUtils;
import com.yunsizhi.mapper.system.SysMenuMapper;
import com.yunsizhi.mapper.system.SysRoleMenuMapper;
import com.yunsizhi.model.dto.SysMenu;
import com.yunsizhi.model.dto.SysRole;
import com.yunsizhi.model.dto.SysRoleMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 菜单 业务层处理
 *
 * @author zjl
 */
@Service
public class SysMenuService extends ServiceImpl<SysMenuMapper, SysMenu> {

    @Autowired
    SysRoleMenuMapper roleMenuMapper;

    /**
     * 权限加入AuthorizationInfo认证对象
     */
    public Set<String> selectPermsByUserId(Long userId) {
        List<String> perms = this.baseMapper.selectPermsByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms) {
            if (StringUtils.isNotEmpty(perm)) {
                permsSet.addAll(Arrays.asList(perm.trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 根据用户ID查询树形菜单
     */
    public List<SysMenu> selectMenusByUserId(Long userId) {

        List<SysMenu> menus = this.baseMapper.selectMenusByUserId(userId);
        return TreeUtils.getChildPerms(menus, 0);

    }

    /**
     * 根据用户ID查询菜单列表
     */
    public List<SysMenu> selectMenusListByUserId(Long userId) {
        return this.baseMapper.selectMenusAllByUserId(userId);
    }


    /**
     * 根据角色ID查询菜单列表
     */
    public List<SysMenu> selectMenuListByRole(Long roleId) {
        return this.baseMapper.selectMenuListByRole(roleId);
    }


    /**
     * 根据角色ID查询菜单 树
     *
     * @param role 角色对象
     * @return 菜单列表
     */
    public List<Map<String, Object>> roleMenuTreeData(SysRole role) {
        Long roleId = role.getId();
        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        List<SysMenu> menuList = this.baseMapper.selectList(null);
        if (StringUtils.isNotNull(roleId)) {
            List<String> roleMenuList = this.baseMapper.selectMenuTree(roleId);
            trees = getTrees(menuList, true, roleMenuList, true);
        } else {
            trees = getTrees(menuList, false, null, true);
        }
        return trees;
    }

    /**
     * 对象转菜单树
     *
     * @param menuList     菜单列表
     * @param isCheck      是否需要选中
     * @param roleMenuList 角色已存在菜单列表
     * @param permsFlag    是否需要显示权限标识
     * @return
     */
    public List<Map<String, Object>> getTrees(List<SysMenu> menuList,
                                              boolean isCheck, List<String> roleMenuList, boolean permsFlag) {
        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        for (SysMenu menu : menuList) {
            Map<String, Object> deptMap = new HashMap<String, Object>();
            deptMap.put("id", menu.getId());
            deptMap.put("pId", menu.getParentId());
            deptMap.put("name", transMenuName(menu, roleMenuList, permsFlag));
            if (isCheck) {
                deptMap.put("checked",
                        roleMenuList.contains(menu.getId() + menu.getPerms()));
            } else {
                deptMap.put("checked", false);
            }
            trees.add(deptMap);
        }
        return trees;
    }

    public String transMenuName(SysMenu menu, List<String> roleMenuList,
                                boolean permsFlag) {
        StringBuffer sb = new StringBuffer();
        sb.append(menu.getMenuName());
        if (permsFlag) {
            sb.append("<font color=\"#888\">&nbsp;&nbsp;&nbsp;"
                    + menu.getPerms() + "</font>");
        }
        return sb.toString();
    }

    /**
     * 查询所有菜单
     * <p>
     * 角色对象
     *
     * @return 菜单列表
     */
    public List<Map<String, Object>> menuTreeData() {
        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        List<SysMenu> menuList = this.baseMapper.selectList(null);
        trees = getTrees(menuList, false, null, false);
        return trees;
    }

    /**
     * 保存菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public int saveMenu(SysMenu menu) {
        Long menuId = menu.getId();
        if (StringUtils.isNotNull(menuId)) {
            menu.setLastOperatorTime(new Date());
            //menu.setLastOperator(ShiroUtils.getLoginName());
            //.setLastOperatorId(ShiroUtils.getUserId());
            return this.baseMapper.updateById(menu);
        } else {
            menu.setCreatorTime(new Date());
            //menu.setCreator(ShiroUtils.getLoginName());
           // menu.setCreatorId(ShiroUtils.getUserId());
            return this.baseMapper.insert(menu);
        }
    }

    /**
     * 查询菜单使用数量
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    public int selectCountRoleMenuByMenuId(Long menuId) {
        return this.roleMenuMapper.selectCount(new QueryWrapper<SysRoleMenu>().eq("menu_id", menuId));
    }
}
