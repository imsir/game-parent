package com.yunsizhi.auth.shiro.realm;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunsizhi.auth.constant.UserConstants;
import com.yunsizhi.auth.service.SysUserService;
import com.yunsizhi.auth.shiro.service.TokenService;
import com.yunsizhi.auth.shiro.token.JwtToken;
import com.yunsizhi.auth.shiro.token.LoginUser;
import com.yunsizhi.common.exception.UserBlockedException;
import com.yunsizhi.model.dto.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

/**
 * @author zjl
 * @date 2019/8/6 10:02
 */
@Slf4j
public class JwtRealm extends AuthorizingRealm {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private SysUserService userService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 只有当需要检测用户权限的时候才会调用此方法，例如checkRole,checkPermission之类的
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        LoginUser loginUser = tokenService.getLoginUser();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        // 添加角色
        authorizationInfo.addRoles(loginUser.getRoleSet());
        // 添加权限
        authorizationInfo.addStringPermissions(loginUser.getPermissionsSet());
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        String token = (String) auth.getCredentials();
        // 获得phone
        String phone = tokenService.getPhone(token);
        log.info(phone + " - token auth start...");
        if (StringUtils.isEmpty(phone)) {
            throw new IncorrectCredentialsException();
        }
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("phone", phone));
        if (user == null) {
            throw new IncorrectCredentialsException();
        }
        try {
            //boolean verify = tokenService.verify(token, user.getPassword());
            if (user.getStatus()== UserConstants.USER_BLOCKED){
                throw new UserBlockedException();
            }
            boolean verify = tokenService.checkToken(token);
            if (verify == Boolean.FALSE) {
                throw new IncorrectCredentialsException();
            }
        }catch (UserBlockedException e){
            throw new UserBlockedException();
        }catch (Exception e) {
            throw new IncorrectCredentialsException();
        }
        return new SimpleAuthenticationInfo(token, token, "JwtRealm");
    }
}
