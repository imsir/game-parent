package com.yunsizhi.auth.shiro.service;


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunsizhi.auth.service.SysMenuService;
import com.yunsizhi.auth.service.SysRoleService;
import com.yunsizhi.auth.service.SysUserService;
import com.yunsizhi.auth.shiro.token.LoginUser;
import com.yunsizhi.common.util.RedisUtil;
import com.yunsizhi.common.util.ServletUtils;
import com.yunsizhi.core.constant.Constant;
import com.yunsizhi.core.constant.RedisKey;
import com.yunsizhi.model.dto.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author zjl
 * @version 1.0
 * @date 2020/4/15 11:45
 */
@Service
public class TokenService {

    @Autowired
    private SysUserService userService;

    @Autowired
    private SysMenuService menuService;

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 获取当前登录的User对象
     * @return User
     */
    public LoginUser getLoginUser(){
        // 获取token
        String token = getToken(ServletUtils.getRequest());
        // 获取手机号
        String phone = getPhone(token);
        // 获取缓存loginUserKey
        String loginUserKey = RedisKey.getLoginUserKey(phone);
        // 获取缓存loginUser
        Object cacheObject = redisUtil.get(loginUserKey);
        if (cacheObject == null) {
            LoginUser loginUser = new LoginUser();
            // 获取当前登录用户
            SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("phone",phone));
            loginUser.setUser(user);
            // 获取当前登录用户所有权限
            Set<String> permissionsSet = menuService.selectPermsByUserId(user.getId());
            loginUser.setPermissionsSet(permissionsSet);
            // 获取当前登录用户所有角色
            Set<String> roleSet = roleService.selectRoleKeys(user.getId());
            loginUser.setRoleSet(roleSet);
            // 缓存当前登录用户
            redisUtil.set(loginUserKey, loginUser, 15L, TimeUnit.MINUTES);
            return loginUser;
        }
        return (LoginUser) cacheObject;
    }


    /**
     * 获得token中的信息无需secret解密也能获得
     * @param token token
     * @return token中包含的用户手机号
     */
    public String getPhone(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("phone").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 获得token中的信息无需secret解密也能获得
     * @param request HttpServletRequest
     * @return token中包含的用户手机号
     */
    public String getPhone(HttpServletRequest request) {
        try {
            DecodedJWT jwt = JWT.decode(this.getToken(request));
            return jwt.getClaim("phone").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }


    /**
     * 获得token中的信息无需secret解密也能获得
     * @param token token
     * @return token中包含的用户id
     */
    public String getUserId(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("userId").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 获得token中的信息无需secret解密也能获得
     * @param request HttpServletRequest
     * @return token中包含的用户id
     */
    public String getUserId(HttpServletRequest request) {
        try {
            DecodedJWT jwt = JWT.decode(this.getToken(request));
            return jwt.getClaim("userId").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }


    /**
     * 获取当前登录用户的token
     * @param request HttpServletRequest
     * @return token
     */
    public String getToken(HttpServletRequest request){
        return request.getHeader(Constant.TOKEN_HEADER_NAME);
    }


    /**
     *
     * @param phone 用户名/手机号
     * @param userId   用户id
     * @param secret   用户的密码
     * @return 加密的token
     */
    public String createToken(String phone, Integer userId, String secret) {
        Date date = new Date(System.currentTimeMillis() + Constant.TOKEN_EXPIRE_TIME);
        Algorithm algorithm = Algorithm.HMAC256(secret);
        return JWT.create()
                .withClaim("phone", phone)
                .withClaim("userId", String.valueOf(userId))
                .withExpiresAt(date)
                .sign(algorithm);
    }

    /**
     * 校验token是否正确
     *
     * @param token  密钥
     * @param secret 用户的密码
     * @return 是否正确
     */
    public boolean verify(String token, String secret) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            String phone = jwt.getClaim("phone").asString();
            String userId = jwt.getClaim("userId").asString();
            // 根据密码生成JWT效验器
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim("phone", phone)
                    .withClaim("userId", String.valueOf(userId))
                    .build();
            // 效验TOKEN
            verifier.verify(token);
            return true;
        } catch (JWTVerificationException exception) {
            return false;
        }
    }

    public static void main(String[] args) {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwaG9uZSI6IjE4MzQ5Mjc4NzM3IiwiZXhwIjoxNjAxNTQwMTE4LCJ1c2VySWQiOiIxIn0.WjcuKB4iWtDpIW9Sns3EyqaoQGi5uexBX7lTA41kWs0";
        DecodedJWT jwt = JWT.decode(token);
        String phone = jwt.getClaim("phone").asString();
        String userId = jwt.getClaim("userId").asString();

        // 根据密码生成JWT效验器
        Algorithm algorithm = Algorithm.HMAC256("184600fe0d788800fec2e8505cfd15c78b275b94585daaa9ae7b709ce5b304fe");
        JWTVerifier verifier = JWT.require(algorithm)
                .withClaim("phone", phone)
                .withClaim("userId", userId)
                .build();
        // 效验TOKEN
        verifier.verify(token);

    }

    /**
     * 验证Token是否过期
     * @param token
     * @return
     */
    public boolean checkToken(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            Date timeoutDate = jwt.getExpiresAt();
            Calendar calendar = Calendar.getInstance();
            Date time = calendar.getTime();
            if(time.before(timeoutDate)){
                return true;
            }
            return false;
        } catch (JWTDecodeException e) {
            e.printStackTrace();
            return false;
        }
    }

}
