package com.yunsizhi.auth.shiro.util;

import com.yunsizhi.auth.constant.AuthConstants;
import com.yunsizhi.auth.service.SysLoginLogService;
import com.yunsizhi.common.util.ServletUtils;
import com.yunsizhi.common.util.SpringUtils;
import com.yunsizhi.model.dto.SysLoginLog;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * 记录用户日志信息
 *
 * @author zjl
 */
@Slf4j
public class SystemLogUtils {

    /**
     * 记录格式 [ip][用户名][操作][错误消息]
     * <p/>
     * 注意操作如下： loginError 登录失败 loginSuccess 登录成功 passwordError 密码错误 changePassword 修改密码 changeStatus 修改状态
     *
     * @param username
     * @param status
     * @param msg
     * @param args
     */
    public static void log(String username, String status, String msg, Object... args) {
        StringBuilder s = new StringBuilder();
        //s.append(LogUtils.getBlock(ShiroUtils.getIp()));
        //s.append(ShiroUtils.getIp());
        s.append(LogUtils.getBlock(username));
        s.append(LogUtils.getBlock(status));
        s.append(LogUtils.getBlock(msg));

        log.info(s.toString(), args);

        if (AuthConstants.LOGIN_SUCCESS.equals(status) || AuthConstants.LOGOUT.equals(status)) {
            saveOpLog(username, msg, AuthConstants.SUCCESS);
        } else if (AuthConstants.LOGIN_FAIL.equals(status)) {
            saveOpLog(username, msg, AuthConstants.FAIL);
        }
    }

    /**
     * 保存日志
     */
    public static void saveOpLog(String username, String message, String status) {
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        // 获取客户端操作系统
        String os = userAgent.getOperatingSystem().getName();
        // 获取客户端浏览器
        String browser = userAgent.getBrowser().getName();
        SysLoginLogService loginLogService = SpringUtils.getBean(SysLoginLogService.class);
        SysLoginLog sysLoginLog = new SysLoginLog();
        sysLoginLog.setLoginName(username);
        sysLoginLog.setStatus(status);
        //sysLoginLog.setIpaddr(ShiroUtils.getIp());
        //sysLoginLog.setLoginLocation(ShiroUtils.getIp());
        sysLoginLog.setBrowser(browser);
        sysLoginLog.setOs(os);
        sysLoginLog.setMsg(message);
        sysLoginLog.setLoginTime(new Date());

        loginLogService.save(sysLoginLog);
    }
}
