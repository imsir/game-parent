package com.yunsizhi.auth.shiro.realm;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunsizhi.auth.constant.UserConstants;
import com.yunsizhi.auth.service.SysUserService;
import com.yunsizhi.auth.shiro.token.CustomizedToken;
import com.yunsizhi.common.exception.UserBlockedException;
import com.yunsizhi.model.dto.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

/**
 * @author zjl
 * @date 2019/7/31 11:40
 */
@Slf4j
public class PasswordRealm extends AuthorizingRealm {

    @Autowired
    private SysUserService userService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof CustomizedToken;
    }

    /**
     * 获取授权信息
     *
     * @param principals principals
     * @return AuthorizationInfo
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    /**
     * 获取身份认证信息
     *
     * @param auth authenticationToken
     * @return AuthenticationInfo
     * @throws AuthenticationException AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        CustomizedToken token = (CustomizedToken) auth;
        log.info(token.getUsername() + " - password auth start...");
        // 根据手机号查询用户
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("phone", token.getUsername()));
        if (user == null) {
            user = userService.getOne(new QueryWrapper<SysUser>().eq("login_name", token.getUsername()));
            if (Objects.isNull(user)){
                // 抛出账号不存在异常
                throw new UnknownAccountException();
            }
        }
        if (user.getStatus()== UserConstants.USER_BLOCKED){
            throw new UserBlockedException();
        }

        // 1.principal：认证的实体信息，可以是手机号，也可以是数据表对应的用户的实体类对象
        // 2.credentials：密码
        Object credentials = user.getPassword();
        // 3.realmName：当前realm对象的name，调用父类的getName()方法即可
        String realmName = super.getName();
        // 4.盐,取用户信息中唯一的字段来生成盐值，避免由于两个用户原始密码相同，加密后的密码也相同
        ByteSource credentialsSalt = ByteSource.Util.bytes(user.getSalt());
        return new SimpleAuthenticationInfo(user, credentials, credentialsSalt, realmName);
    }
}
