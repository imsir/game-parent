package com.yunsizhi.auth.shiro.token;

import com.yunsizhi.model.dto.SysUser;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * @author zjl
 * @version 1.0
 * @date 2020/4/17 14:23
 */
@Data
@NoArgsConstructor
public class LoginUser {

    /**
     * 角色列表
     */
    private Set<String> roleSet;

    /**
     * 权限列表
     */
    private Set<String> permissionsSet;

    /**
     * User
     */
    private SysUser user;
}
