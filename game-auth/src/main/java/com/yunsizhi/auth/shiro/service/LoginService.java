package com.yunsizhi.auth.shiro.service;


import com.aliyuncs.exceptions.ClientException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunsizhi.auth.constant.LoginType;
import com.yunsizhi.auth.service.SysUserService;
import com.yunsizhi.auth.shiro.token.CustomizedToken;
import com.yunsizhi.auth.shiro.util.CommonsUtils;
import com.yunsizhi.common.enums.GlobalCodeEnum;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.common.util.RedisUtil;
import com.yunsizhi.core.constant.Constant;
import com.yunsizhi.core.constant.RedisKey;
import com.yunsizhi.model.dto.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author zjl
 * @version 1.0
 * @date 2020/4/15 16:50
 */
@Service
public class LoginService {

    @Autowired
    private SysUserService userService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private TokenService tokenService;


    public boolean sendLoginCode(String phone) throws ClientException {
        // 这里使用默认值，随机验证码的方法为CommonsUtils.getCode()
        int code = 6666;
        // todo 此处为发送验证码代码
//        SmsUtils.sendSms(phone, code);
        // 将验证码加密后存储到redis中
        String encryptCode = CommonsUtils.encryptPassword(String.valueOf(code), phone);
        redisUtil.set(RedisKey.getLoginCodeKey(phone), encryptCode, Constant.CODE_EXPIRE_TIME, TimeUnit.MINUTES);
        return true;
    }


    public boolean sendModifyPasswordCode(String phone) throws ClientException {
        int code = 6666;
        // todo 此处为发送验证码代码
//        SmsUtils.sendSms(phone, code);
        redisUtil.set(RedisKey.getModifyPasswordCodeKey(phone), code, Constant.CODE_EXPIRE_TIME, TimeUnit.MINUTES);
        return true;
    }


    /**
     * 密码登录校验
     *
     * @param phone
     * @param password
     * @return
     */
    public JsonResult loginByPassword(String phone, String password) {
        // 1.获取Subject
        Subject subject = SecurityUtils.getSubject();
        // 2.封装用户数据
        CustomizedToken token = new CustomizedToken(phone, password, LoginType.PASSWORD_LOGIN_TYPE.toString());
        // 3.执行登录方法
        try {
            subject.login(token);

            SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("phone", token.getUsername()));
            if (user == null) {
                user = userService.getOne(new QueryWrapper<SysUser>().eq("login_name", token.getUsername()));
            }

            return JsonResult.success(returnLoginInitParam(user.getPhone()));
        } catch (UnknownAccountException e) {
            return JsonResult.error(GlobalCodeEnum.USERNAME_NOT_EXIST);
        } catch (IncorrectCredentialsException e) {
            return JsonResult.error(GlobalCodeEnum.PASSWORD_ERROR);
        }
    }


    /**
     * 验证码登录校验
     *
     * @param phone
     * @param code
     * @return
     */
    public JsonResult loginByCode(String phone, String code) {
        // 1.获取Subject
        Subject subject = SecurityUtils.getSubject();
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("phone", phone));
        // 2.验证码登录，如果该用户不存在则创建该用户
        if (Objects.isNull(user)) {
            // 2.1 注册 或者返回错误
            return JsonResult.error(GlobalCodeEnum.USERNAME_NOT_EXIST);
        }
        // 3.封装用户数据
        CustomizedToken token = new CustomizedToken(phone, code, LoginType.CODE_LOGIN_TYPE.toString());
        // 4.执行登录方法
        try {
            subject.login(token);
            return JsonResult.success(returnLoginInitParam(phone));
        } catch (UnknownAccountException e) {
            return JsonResult.error(GlobalCodeEnum.USERNAME_NOT_EXIST);
        } catch (ExpiredCredentialsException e) {
            return JsonResult.error(GlobalCodeEnum.CODE_EXPIRE);
        } catch (IncorrectCredentialsException e) {
            return JsonResult.error(GlobalCodeEnum.CODE_ERROR);
        }
    }


    public JsonResult modifyPassword(String phone, String code, String password) {
        Object modifyCode = redisUtil.get(RedisKey.getModifyPasswordCodeKey(phone));
        // 判断redis中是否存在验证码
        if (Objects.isNull(modifyCode)) {
            return JsonResult.error(GlobalCodeEnum.CODE_EXPIRE);
        }
        // 判断redis中code与传递过来的code 是否相等
        if (!Objects.equals(code, modifyCode.toString())) {
            return JsonResult.error(GlobalCodeEnum.CODE_ERROR);
        }
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("phone", phone));
        // 如果用户不存在，执行注册
        if (Objects.isNull(user)) {
            Boolean flag = true;
            if (flag) {

                return JsonResult.success(this.returnLoginInitParam(phone));
            } else {
                return JsonResult.error();
            }
        }
        String salt = CommonsUtils.uuid();
        String encryptPassword = CommonsUtils.encryptPassword(password, salt);
        user.setSalt(salt);
        user.setPassword(encryptPassword);
        // 删除缓存
        redisUtil.deleteObject(RedisKey.getLoginUserKey(phone));
        boolean flag = userService.updateById(user);
        if (flag) {
            return JsonResult.success(this.returnLoginInitParam(phone));
        } else {
            return JsonResult.error();
        }
    }


    /**
     * 返回登录后初始化参数
     *
     * @param phone phone
     * @return Map<String                               ,                                                               Object>
     */
    private Map<String, Object> returnLoginInitParam(String phone) {
        Map<String, Object> data = new HashMap<>(1);
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("phone", phone));
        // 生成jwtToken
        String token = tokenService.createToken(phone, user.getId().intValue(), user.getPassword());
        // token
        data.put("token", token);
        return data;
    }

}
