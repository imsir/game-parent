package com.yunsizhi.auth.aspectj;

import com.alibaba.fastjson.JSONObject;
import com.yunsizhi.auth.shiro.service.TokenService;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.common.util.HttpHelper;
import com.yunsizhi.common.util.IpUtils;
import com.yunsizhi.common.util.RedisUtil;
import com.yunsizhi.common.util.StringUtils;
import com.yunsizhi.core.constant.Constant;
import com.yunsizhi.core.constant.RedisKey;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *
 */
@Aspect
@Component
@Slf4j
public class RepeatSubmitAspect {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private TokenService tokenService;

    private final String REPEAT_PARAMS = "repeatParams";

    private final String REPEAT_TIME = "repeatTime";

    /**
     * @param point
     */
    @Around("@annotation(com.yunsizhi.core.annotation.RepeatSubmit)")
    public Object around(ProceedingJoinPoint point) throws Throwable {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String ip = IpUtils.getIpAddr(request);

        String nowParams = HttpHelper.getBodyString(request);

        String token = request.getHeader(Constant.TOKEN_HEADER_NAME);

        // body参数为空，获取Parameter的数据
        if (StringUtils.isEmpty(nowParams)) {
            nowParams = JSONObject.toJSONString(request.getParameterMap());
        }

        //获取注解
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        //目标类、方法
        String className = method.getDeclaringClass().getName();
        String name = method.getName();
        //String ipKey = String.format("%s#%s", className, name);
        //int hashCode = Math.abs(ipKey.hashCode());

        String formRepeatKey = RedisKey.getFormRepeatKey();
        //String key = formRepeatKey + String.format("%s_%d", ip, hashCode);

        //log.info("ipKey={},hashCode={},key={}", ipKey, hashCode, key);
        String url = request.getRequestURI();
        String phone = tokenService.getPhone(token);
        url += ":"+phone;

        Map<String, Object> nowDataMap = new HashMap<>(2);
        nowDataMap.put(REPEAT_PARAMS, nowParams);
        nowDataMap.put(REPEAT_TIME, System.currentTimeMillis());

        Object sessionObj = redisUtil.get(formRepeatKey);
        if (sessionObj != null) {
            Map<String, Object> sessionMap = (Map<String, Object>) sessionObj;
            if (sessionMap.containsKey(url)) {
                Map<String, Object> preDataMap = (Map<String, Object>) sessionMap.get(url);
                if (compareParams(nowDataMap, preDataMap) && compareTime(nowDataMap, preDataMap)) {
                    return JsonResult.error("请勿重复提交");
                }
            }
        }
        Map<String, Object> cacheMap = new HashMap<>(2);
        cacheMap.put(url, nowDataMap);
        redisUtil.set(formRepeatKey, cacheMap, Constant.FORM_REPEAT_TIME, TimeUnit.SECONDS);
        //执行方法
        Object object = point.proceed();
        return object;
    }

    /**
     * 判断参数是否相同
     */
    private boolean compareParams(Map<String, Object> nowMap, Map<String, Object> preMap) {
        String nowParams = (String) nowMap.get(REPEAT_PARAMS);
        String preParams = (String) preMap.get(REPEAT_PARAMS);
        return nowParams.equals(preParams);
    }

    /**
     * 判断两次间隔时间
     */
    private boolean compareTime(Map<String, Object> nowMap, Map<String, Object> preMap) {
        long time1 = (Long) nowMap.get(REPEAT_TIME);
        long time2 = (Long) preMap.get(REPEAT_TIME);
        return (time1 - time2) < (Constant.FORM_REPEAT_TIME * 1000);
    }

}
