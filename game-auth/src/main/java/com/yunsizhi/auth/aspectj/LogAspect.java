//package com.yunsizhi.auth.aspectj;
//
//import com.alibaba.fastjson.JSONObject;
//import com.yunsizhi.auth.service.SysOperLogService;
//import com.yunsizhi.common.enums.GlobalCodeEnum;
//import com.yunsizhi.common.result.JsonResult;
//import com.yunsizhi.common.util.ServletUtils;
//import com.yunsizhi.common.util.StringUtils;
//import com.yunsizhi.core.annotation.Log;
//import com.yunsizhi.model.dto.SysOperLog;
//import com.yunsizhi.model.dto.SysUser;
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.Signature;
//import org.aspectj.lang.annotation.AfterReturning;
//import org.aspectj.lang.annotation.AfterThrowing;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.aspectj.lang.reflect.MethodSignature;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.scheduling.annotation.EnableAsync;
//import org.springframework.stereotype.Component;
//
//import java.lang.reflect.Method;
//import java.util.Date;
//import java.util.Map;
//
///**
// * 操作日志记录处理
// *
// * @author zjl
// */
//@Aspect
//@Component
//@EnableAsync
//@Slf4j
//public class LogAspect {
//    @Autowired
//    private SysOperLogService operLogService;
//
//    @Pointcut("@annotation(com.yunsizhi.core.annotation.Log)")
//    public void logPointCut() {
//    }
//
//    /**
//     * 前置通知 用于拦截操作
//     *
//     * @param joinPoint 切点
//     */
//    @AfterReturning(pointcut = "logPointCut()", returning = "result")
//    public void doBefore(JoinPoint joinPoint, Object result) {
//        handleLog(joinPoint, null, result);
//    }
//
//    /**
//     * 拦截异常操作
//     *
//     * @param joinPoint
//     * @param e
//     */
//    @AfterThrowing(value = "logPointCut()", throwing = "e")
//    public void doAfter(JoinPoint joinPoint, Exception e) {
//        handleLog(joinPoint, e, null);
//    }
//
//    @Async
//    protected void handleLog(final JoinPoint joinPoint, final Exception e, final Object result) {
//        try {
//            // 获得注解
//            Log controllerLog = getAnnotationLog(joinPoint);
//            if (controllerLog == null) {
//                return;
//            }
//
//            // 获取当前的用户
//            SysUser currentUser = ShiroUtils.getUser();
//
//            // *========数据库日志=========*//
//            SysOperLog operLog = new SysOperLog();
//            operLog.setStatus(GlobalCodeEnum.STATUS_OK.getCode());
//            // 请求的地址
//            String ip = ShiroUtils.getIp();
//            operLog.setOperIp(ip);
//            // 操作地点
//            //operLog.setOperLocation(AddressUtils.getRealAddressByIP(ip));
//
//            operLog.setOperUrl(ServletUtils.getRequest().getRequestURI());
//            if (currentUser != null) {
//                operLog.setOperName(currentUser.getLoginName());
//            }
//            if (StringUtils.isNotNulls(e) && StringUtils.isNotNulls(e.getMessage())) {
//                operLog.setStatus(GlobalCodeEnum.STATUS_FAIL.getCode());
//                operLog.setErrorMsg(StringUtils.substring(e.getMessage(), 0, 2000));
//            }
//
//            if (null != result) {
//                if (result instanceof JsonResult) {
//                    JsonResult ajaxResult = (JsonResult) result;
//                    if (!"200".equals(ajaxResult.get("code").toString())) {
//                        operLog.setStatus(GlobalCodeEnum.STATUS_FAIL.getCode());
//                    }
//                } else {
//                    operLog.setStatus(GlobalCodeEnum.STATUS_FAIL.getCode());
//                }
//            }
//            // 设置方法名称
//            String className = joinPoint.getTarget().getClass().getName();
//            String methodName = joinPoint.getSignature().getName();
//            operLog.setMethod(className + "." + methodName + "()");
//            // 处理设置注解上的参数
//            getControllerMethodDescription(controllerLog, operLog);
//            operLog.setOperTime(new Date());
//            // 保存数据库
//            operLogService.save(operLog);
//        } catch (Exception exp) {
//            // 记录本地异常日志
//            log.error("==前置通知异常==");
//            log.error("异常信息:{}", exp.getMessage());
//            exp.printStackTrace();
//        }
//    }
//
//    /**
//     * 获取注解中对方法的描述信息 用于Controller层注解
//     *
//     * @return 方法描述
//     * @throws Exception
//     */
//    public void getControllerMethodDescription(Log log, SysOperLog operLog)
//            throws Exception {
//        // 设置action动作
//        operLog.setAction(log.action());
//        // 设置标题
//        operLog.setTitle(log.title());
//        // 设置channel
//        operLog.setChannel(log.channel());
//        // 是否需要保存request，参数和值
//        if (log.isSaveRequestData()) {
//            // 获取参数的信息，传入到数据库中。
//            setRequestValue(operLog);
//        }
//    }
//
//    /**
//     * 获取请求的参数，放到log中
//     *
//     * @param operLog
//     */
//    private void setRequestValue(SysOperLog operLog) {
//        Map<String, String[]> map = ServletUtils.getRequest().getParameterMap();
//        String params = JSONObject.toJSONString(map);
//        operLog.setOperParam(StringUtils.substring(params, 0, 255));
//    }
//
//    /**
//     * 是否存在注解，如果存在就获取
//     */
//    private Log getAnnotationLog(JoinPoint joinPoint) throws Exception {
//        Signature signature = joinPoint.getSignature();
//        MethodSignature methodSignature = (MethodSignature) signature;
//        Method method = methodSignature.getMethod();
//
//        if (method != null) {
//            return method.getAnnotation(Log.class);
//        }
//        return null;
//    }
//}
