package com.yunsizhi.admin.controller.system;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunsizhi.admin.intercepter.LoginRequired;
import com.yunsizhi.auth.base.BaseController;
import com.yunsizhi.auth.constant.BusinessType;
import com.yunsizhi.auth.service.SysMenuService;
import com.yunsizhi.auth.service.SysRoleService;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.core.annotation.Log;
import com.yunsizhi.model.dto.SysRole;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 角色信息
 *
 * @author zjl
 */
@Controller
@RequestMapping("/api/role")
public class ApiSysRoleController extends BaseController {


    @Autowired
    private SysRoleService roleService;
    @Autowired
    private SysMenuService menuService;

    /**
     * api角色列表获取
     *
     * @param role
     * @return
     */
    @RequiresPermissions("api:role:list")
    @PostMapping("/list")
    @ResponseBody
    public JsonResult list(SysRole role) {
        startPage();
        role.setDeleted(0);
        List<SysRole> list = roleService.list(new QueryWrapper<>(role));
        return success(getDataTable(list));
    }


    /**
     * api通过角色ID获取角色下的菜单
     */
    @Log(title = " pi通过角色ID获取角色下的菜单", action = BusinessType.SELECT)
    @GetMapping("/getMenuByRole")
    @LoginRequired
    public JsonResult getMenuByRole(Long roleId) {
        return success(menuService.selectMenuListByRole(roleId));
    }

    /**
     * api角色保存
     */
    @RequiresPermissions("api:role:save")
    @Log(title = " api角色保存", action = BusinessType.SAVE)
    @PostMapping("/save")
    @LoginRequired
    public JsonResult save(SysRole role) {
        if (roleService.saveRole(role) > 0) {
            return success();
        }
        return error();
    }

    /**
     * api角色逻辑删除
     *
     * @param ids
     * @return
     */
    @RequiresPermissions("api:role:remove")
    @Log(title = " api角色逻辑删除", action = BusinessType.DELETE)
    @PostMapping("/remove")
    @LoginRequired
    public JsonResult remove(String ids) {
        try {
            roleService.deleteRoleByIds(ids);
            return success();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /**
     * 校验角色名称
     */
    @PostMapping("/checkRoleNameUnique")
    @LoginRequired
    public String checkRoleNameUnique(Long roleId, String roleName) {
        String uniqueFlag = "0";
        if (roleName != null) {
            SysRole temp = roleService.getOne(new QueryWrapper<SysRole>().eq("role_name", roleName).eq("is_del", "0"));
            if (temp != null) {
                if (roleId.longValue() == temp.getId().longValue()) {
                    return "0";
                } else {
                    return "1";
                }
            }
        }
        return uniqueFlag;
    }

}
