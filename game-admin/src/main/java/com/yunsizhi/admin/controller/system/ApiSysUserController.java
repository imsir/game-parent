package com.yunsizhi.admin.controller.system;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunsizhi.admin.intercepter.LoginRequired;
import com.yunsizhi.auth.base.BaseController;
import com.yunsizhi.auth.constant.BusinessType;
import com.yunsizhi.auth.service.SysMenuService;
import com.yunsizhi.auth.service.SysRoleService;
import com.yunsizhi.auth.service.SysUserService;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.common.util.StringUtils;
import com.yunsizhi.core.annotation.Log;
import com.yunsizhi.core.annotation.RepeatSubmit;
import com.yunsizhi.model.dto.SysMenu;
import com.yunsizhi.model.dto.SysRole;
import com.yunsizhi.model.dto.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 用户信息
 *
 * @author zjl
 */
@Api(tags = "用户管理相关接口")
@RestController
@RequestMapping("/api/user")
public class ApiSysUserController extends BaseController {

    @Autowired
    private SysUserService userService;

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private SysMenuService sysMenuService;


    /**
     * api获取当前用户信息
     */
    @ApiOperation("通过手机验证码修改密码")
    @RequiresPermissions("api:user:getUserInfo")
    @GetMapping("/getUserInfo")
    @LoginRequired
    @RepeatSubmit
    public JsonResult getUserInfo() {
        JsonResult result = new JsonResult();
        SysUser user = getUser();
        if (StringUtils.isNotNulls(user)) {
            List<SysMenu> menus = sysMenuService.selectMenusListByUserId(user.getId());
            result.put("code", "200");
            result.put("msg", "获取数据成功");
            result.put("user", user);
            result.put("menus", menus);

        } else {
            result.put("code", "500");
            result.put("msg", "获取数据失败");
        }
        return result;
    }

    /**
     * api用户列表获取
     *
     * @param user
     * @return
     */
    @RequiresPermissions("api:user:list")
    @PostMapping("/list")
    @LoginRequired
    public JsonResult list(SysUser user) {
        startPage();
        user.setDeleted(0);
        List<SysUser> list = userService.selectUserList(user);
        return success(getDataTable(list));
    }


    /**
     * api获取所有角色列表
     */
    @RequiresPermissions("api:user:role")
    @GetMapping("/getRoleList")
    @LoginRequired
    public JsonResult getRoleList() {
        List<SysRole> roles = roleService.list();
        return success(roles);
    }

    /**
     * api用户重置密码
     *
     * @param user
     * @return
     */
    @RepeatSubmit
    @RequiresPermissions("api:user:resetPwd")
    @Log(title = "api用户重置密码", action = BusinessType.SAVE)
    @PostMapping("/resetPwd")
    @LoginRequired
    public JsonResult resetPwd(SysUser user) {
        user.setPassword("abc123456");
        int rows = userService.resetUserPwd(user);
        if (rows > 0) {
            return success();
        }
        return error();
    }

    /**
     * api用户逻辑删除
     *
     * @param ids
     * @return
     */
    @RequiresPermissions("api:user:remove")
    @Log(title = "api用户逻辑删除", action = BusinessType.DELETE)
    @PostMapping("/remove")
    @LoginRequired
    public JsonResult remove(String ids) {
        try {
            userService.deleteUserByIds(ids);
            return success();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /**
     * api用户保存
     */
    @RequiresPermissions("api:user:save")
    @Log(title = "api用户保存", action = BusinessType.SAVE)
    @PostMapping("/save")
    @LoginRequired
    public JsonResult save(SysUser user) {
        if (user.getId() != null && user.getId().longValue() == getUser().getId().longValue() && StringUtils.isNotNulls(user.getStatus()) && "1".equals(user.getStatus())) {
            return error("不允许停用自己");
        }
        if (StringUtils.isNotNull(user.getId()) && SysUser.isAdmin(user.getId())) {
            return error("不允许修改超级管理员用户");
        }
        if (userService.saveUser(user) > 0) {

            return success();
        }
        return error();
    }

    /**
     * 保存头像
     */
    @Log(title = "个人信息", action = BusinessType.SAVE)
    @PostMapping("/uploadImg")
    @LoginRequired
    public JsonResult uploadImg(@RequestParam("file") MultipartFile file) {
        try {
            if (!file.isEmpty()) {
                //String avatar = FileUploadUtils.upload(file);
                //Map<String, String> urlMap = new HashMap<String, String>();
                //urlMap.put("src", avatar);
                return success();
            }
            return error();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /**
     * 校验用户名
     */
    @PostMapping("/checkLoginNameUnique")
    @LoginRequired
    public String checkLoginNameUnique(String loginName) {
        SysUser user = getUser();
        String uniqueFlag = "0";
        if (user != null) {
            SysUser temp = userService.getOne(new QueryWrapper<SysUser>().eq("login_name", loginName).eq("is_del", "0"));
            if (temp != null) {
                if (user.getId().longValue() == temp.getId().longValue()) {
                    return "0";
                } else {
                    return "1";
                }
            }

        }
        return uniqueFlag;
    }

    /**
     * 校验手机号码
     */
    @PostMapping("/checkPhoneUnique")
    @LoginRequired
    public String checkPhoneUnique(String phoneNumber) {
        SysUser user = getUser();
        String uniqueFlag = "0";
        if (user != null) {
            if (user != null) {
                SysUser temp = userService.getOne((new QueryWrapper<SysUser>().eq("phonenumber", phoneNumber).eq("is_del", "0")));
                if (temp != null) {
                    if (user.getId().longValue() == temp.getId().longValue()) {
                        return "0";
                    } else {
                        return "1";
                    }
                }
            }
        }
        return uniqueFlag;
    }

    /**
     * 校验email邮箱
     */
    @PostMapping("/checkEmailUnique")
    @LoginRequired
    public String checkEmailUnique(String email) {
        SysUser user = getUser();
        String uniqueFlag = "0";
        if (user != null) {
            SysUser temp = userService.getOne((new QueryWrapper<SysUser>().eq("email", email).eq("is_del", "0")));
            if (temp != null) {
                if (user.getId().longValue() == temp.getId().longValue()) {
                    return "0";
                } else {
                    return "1";
                }
            }
        }
        return uniqueFlag;
    }
}
