package com.yunsizhi.admin.controller.system;


import com.aliyuncs.exceptions.ClientException;
import com.yunsizhi.admin.intercepter.LoginRequired;
import com.yunsizhi.auth.base.BaseController;
import com.yunsizhi.auth.shiro.service.LoginService;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.model.dto.MdbTest;
import com.yunsizhi.service.mongo.MdbTestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 用户登录退出接口
 *
 * @author zjl
 */
@Api(tags = "用户登录相关接口")
@RestController
@RequestMapping("/admin/login")
@Validated
public class ApiLoginController extends BaseController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private MdbTestService mdbTestService;

    /**
     * 退出登录
     *
     * @return
     */
    @ApiOperation(value = "退出登录", response = JsonResult.class)
    @PostMapping("/out")
    @LoginRequired
    public JsonResult logout() {
        return success("退出成功");
    }

    /**
     * 发送登录验证码
     *
     * @param phone
     * @return
     * @throws ClientException
     */
    @ApiOperation(value = "发送登录验证码", response = JsonResult.class)
    @ApiImplicitParam(name = "phone", value = "手机号", required = true, paramType = "query")
    @GetMapping("/code")
    public JsonResult sendLoginCode(@NotNull(message = "手机号不能为空") String phone) throws ClientException {
        if (loginService.sendLoginCode(phone)) {
            return JsonResult.success();
        } else {
            return JsonResult.error();
        }
    }


    /**
     * 发送修密码验证码
     *
     * @param phone
     * @return
     * @throws ClientException
     */
    @ApiOperation(value = "发送修改密码验证码", response = JsonResult.class)
    @ApiImplicitParam(name = "phone", value = "手机号", required = true, paramType = "query")
    @GetMapping("/modifyPwdCode")
    public JsonResult sendModifyPasswordCode(@NotEmpty(message = "手机号不能为空") String phone) throws ClientException {
        if (loginService.sendModifyPasswordCode(phone)) {
            return success();
        } else {
            return error();
        }
    }


    /**
     * 修改密码
     *
     * @param phone
     * @param code
     * @param password
     * @return
     */
    @ApiOperation(value = "通过手机验证码修改密码", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", required = true, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "code", value = "验证码", required = true, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, paramType = "query", dataType = "string")
    })
    @PutMapping("/modifyPassword")
    public JsonResult modifyPassword(@NotEmpty(message = "手机号不能为空") String phone,
                                     @NotEmpty(message = "验证码不能为空") String code,
                                     @NotEmpty(message = "密码不能为空") String password) {
        return loginService.modifyPassword(phone, code, password);
    }


    /**
     * 密码登录
     *
     * @param loginName
     * @param password
     * @return
     */
    @ApiOperation(value = "密码登录", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginName", value = "登录帐号(手机号或帐号)", required = true, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, paramType = "query", dataType = "string")
    })
    @PostMapping("/password")
    public JsonResult loginByPassword(@NotEmpty(message = "帐号不能为空") String loginName,
                                      @NotEmpty(message = "密码不能为空") String password) {
        return loginService.loginByPassword(loginName, password);
    }


    /**
     * 验证码登录
     *
     * @param phone
     * @param code
     * @return
     */
    @ApiOperation(value = "验证码登录", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", required = true, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "code", value = "验证码", required = true, paramType = "query", dataType = "string"),
    })
    @PostMapping("/code")
    public JsonResult loginByCode(@NotEmpty(message = "手机号不能为空") String phone,
                                  @NotEmpty(message = "验证码不能为空") String code) {
        return loginService.loginByCode(phone, code);
    }

    @GetMapping("/mdbTest")
    public JsonResult mdbTest(@NotEmpty(message = "手机号不能为空") String phone) {
        MdbTest mdbTest = new MdbTest();
        mdbTest.setId(1L);
        mdbTest.setNickname("张三");
        mdbTest.setTel(phone);
        mdbTestService.save(mdbTest);
        return success(mdbTestService.getByTel(phone));
    }


}
