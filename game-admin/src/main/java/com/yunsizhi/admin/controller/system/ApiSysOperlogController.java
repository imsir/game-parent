package com.yunsizhi.admin.controller.system;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunsizhi.admin.intercepter.LoginRequired;
import com.yunsizhi.auth.base.BaseController;
import com.yunsizhi.auth.service.SysOperLogService;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.common.support.Convert;
import com.yunsizhi.model.dto.SysOperLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * 操作日志记录
 *
 * @author zjl
 */
@RestController
@RequestMapping("/api/operlog")
public class ApiSysOperlogController extends BaseController {

    @Autowired
    private SysOperLogService operLogService;

    /**
     * api用户操作日志列表获取
     *
     * @param operLog
     * @return
     */
    @RequiresPermissions("api:operlog:list")
    @PostMapping("/list")
    @LoginRequired
    public JsonResult list(SysOperLog operLog) {
        startPage();
        operLog.setIsDel(0);
        List<SysOperLog> list = operLogService.list(new QueryWrapper<>(operLog));
        return success(getDataTable(list));
    }

    /**
     * api用户操作日志删除
     *
     * @param ids
     * @return
     */
    @RequiresPermissions("api:operlog:remove")
    @PostMapping("/remove")
    @LoginRequired
    public JsonResult remove(String ids) {
        try {
            List<Integer> idsList = Arrays.asList(Convert.toIntArray(ids));
            SysOperLog temp = new SysOperLog();
            temp.setIsDel(1);
            for (Integer id : idsList) {
                temp.setOperId(id);
                operLogService.updateById(temp);
            }
            return success();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return error();
    }

}
