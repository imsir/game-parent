package com.yunsizhi.admin.controller.system;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunsizhi.admin.intercepter.LoginRequired;
import com.yunsizhi.auth.base.BaseController;
import com.yunsizhi.auth.constant.BusinessType;
import com.yunsizhi.auth.service.SysMenuService;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.common.util.StringUtils;
import com.yunsizhi.core.annotation.Log;
import com.yunsizhi.model.dto.SysMenu;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 菜单信息
 *
 * @author zjl
 */
@Api(tags = "菜单信息")
@RestController
@RequestMapping("/api/menu")
public class ApiSysMenuController extends BaseController {

    @Autowired
    private SysMenuService menuService;

    /**
     * 菜单列表获取MC
     *
     * @return
     */
    @ApiOperation(value = "菜单列表获取", response = SysMenu.class)
    @Log(title = "api菜单列表获取", action = BusinessType.SELECT)
    @RequiresPermissions("api:menu:list")
    @GetMapping("/list")
    @LoginRequired
    public JsonResult list() {
        List<SysMenu> menuList = menuService.list(new QueryWrapper<SysMenu>().eq("is_del", "0").orderByDesc("order_num"));
        return success(menuList);
    }

    /**
     * 获取所有菜单列表
     */
    @Log(title = " api获取所有菜单列表", action = BusinessType.SELECT)
    @GetMapping("/getMenuList")
    @LoginRequired
    public JsonResult getMenuList() {
        return success(menuService.list());
    }


    /**
     * api菜单目录移动
     *
     * @return
     */
    @Log(title = "api菜单目录移动", action = BusinessType.UPDATE)
    @RequiresPermissions("api:menu:move")
    @PostMapping("/move")
    @LoginRequired
    public JsonResult move(String id, String parentId) {
        SysMenu pmenu = menuService.getById(parentId);
        if (StringUtils.isNotNulls(pmenu)) {
            SysMenu menu = new SysMenu();
            menu.setId(Long.parseLong(id));
            menu.setParentId(Long.parseLong(parentId));
            menuService.updateById(menu);
            return success();
        } else {
            return error();
        }
    }


    /**
     * api菜单逻辑删除
     *
     * @param menuId
     * @return
     */
    @Log(title = "api菜单逻辑删除", action = BusinessType.DELETE)
    @RequiresPermissions("api:menu:remove")
    @PostMapping("/remove")
    @LoginRequired
    public JsonResult remove(Long menuId) {

        if (menuService.count(new QueryWrapper<SysMenu>().eq("parent_id", menuId)) > 0) {
            return error("存在子菜单,不允许删除");
        }
//        if (menuService.selectCountRoleMenuByMenuId(menuId) > 0) {
//            return error("500", "菜单已分配,不允许删除");
//        }
        if (menuService.removeById(menuId)) {
            return success();
        }
        return error();
    }

    /**
     * api菜单保存
     *
     * @param menu
     * @return
     */
    @Log(title = "api菜单保存", action = BusinessType.SAVE)
    @RequiresPermissions("api:menu:save")
    @PostMapping("/save")
    @LoginRequired
    public JsonResult save(SysMenu menu) {

        if (StringUtils.isEmpty(menu.getId())) {
            menu.setMenuType("M");
        }
        List<SysMenu> tempList = menuService.list(new QueryWrapper<SysMenu>().eq("remark", menu.getRemark()));
        if (StringUtils.isNotNulls(tempList) && tempList.size() > 0) {
            if (tempList.get(0).getId().longValue() != menu.getId().longValue()) {
                return error("菜单编码重复");
            }
        }

        if (menuService.saveMenu(menu) > 0) {
            return success();
        }
        return error();
    }
}
