package com.yunsizhi.admin.controller.mongo;


import com.yunsizhi.auth.base.BaseController;
import com.yunsizhi.auth.constant.BusinessType;
import com.yunsizhi.common.base.page.PageHelper;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.common.support.Convert;
import com.yunsizhi.common.util.LongUtil;
import com.yunsizhi.core.annotation.Log;
import com.yunsizhi.model.dto.MongoDemoDTO;
import com.yunsizhi.service.mongo.MongoDemoService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * 物品管理
 *
 * @author zjl
 */
@Api(tags = "游戏宠物管理接口")
@RestController
@RequestMapping("/mongo/demo")
@Validated
public class MongDemoController extends BaseController {

    @Autowired
    private MongoDemoService mongoDemoService;

    /**
     * 列表查询数据
     *
     * @param bean
     * @return
     */
    @PostMapping("/list")
    @RequiresPermissions("mongo:demo:list")
    public JsonResult list(MongoDemoDTO bean) {
        bean.setDeleted(false);
        bean.setDisable(false);
        PageHelper page = mongoDemoService.findByPage(initPage(), bean);
        return success(page);
    }

    /**
     * 获取信息
     *
     * @param id
     * @return
     */
    @Log(title = "获取信息", action = BusinessType.QUERY)
    @RequiresPermissions("mongo:demo:query")
    @GetMapping("/get/{id}")
    public JsonResult get(@PathVariable("id") Long id) {
        MongoDemoDTO bean = mongoDemoService.findById(id);
        if (Objects.nonNull(bean)) {
            return success(bean);
        }
        return error();
    }

    /**
     * 添加
     *
     * @param bean
     * @return
     */
    @Log(title = "添加", action = BusinessType.ADD)
    @RequiresPermissions("mongo:demo:add")
    @PostMapping("/add")
    public JsonResult add(MongoDemoDTO bean) {
        //新增
        bean.setId(LongUtil.getId());
        bean.setDeleted(false);
        bean.setDisable(false);
        bean.addProperties();
        bean.setCreator(getLoginName());
        bean.setCreatorId(getUserId());
        mongoDemoService.add(bean);
        return success();
    }

    /**
     * 修改
     *
     * @param bean
     * @return
     */
    @Log(title = "修改", action = BusinessType.UPDATE)
    @RequiresPermissions("mongo:demo:update")
    @PostMapping("/update")
    public JsonResult update(MongoDemoDTO bean) {
        bean.setLastOperator(getLoginName());
        bean.setLastOperatorId(getUserId());
        bean.addProperties();
        mongoDemoService.update(bean);
        return success();
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    @Log(title = "删除", action = BusinessType.DELETE)
    @RequiresPermissions("mongo:demo:remove")
    @PostMapping("/remove")
    public JsonResult remove(String ids) {
        List<Long> idList = Arrays.asList(Convert.toLongArray(ids));
        mongoDemoService.logicalRemove(idList);
        return error();
    }


}
