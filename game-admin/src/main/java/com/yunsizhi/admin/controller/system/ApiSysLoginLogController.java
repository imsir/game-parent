package com.yunsizhi.admin.controller.system;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yunsizhi.admin.intercepter.LoginRequired;
import com.yunsizhi.auth.base.BaseController;
import com.yunsizhi.auth.service.SysLoginLogService;
import com.yunsizhi.common.base.page.TableDataInfo;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.common.support.Convert;
import com.yunsizhi.core.annotation.RepeatSubmit;
import com.yunsizhi.model.dto.SysLoginLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * 系统访问记录
 *
 * @author zjl
 */
@Slf4j
@Api(tags = "系统访问记录")
@RestController
@RequestMapping("/api/loginlog")
public class ApiSysLoginLogController extends BaseController {

    @Autowired
    private SysLoginLogService loginlogService;

    /**
     * 用户登录日志列表获取
     *
     * @param loginlog
     * @return
     */
    @ApiOperation(value = "用户登录日志列表获取", response = TableDataInfo.class)
    @ApiImplicitParam(name = "loginName", value = "登录帐号", required = false, paramType = "query")
    @RequiresPermissions("api:loginlog:list")
    @PostMapping("/list")
    @LoginRequired
    @RepeatSubmit
    public JsonResult list(SysLoginLog loginlog) {
        startPage();
        loginlog.setIsDel(0);
        List<SysLoginLog> list = loginlogService.list(new QueryWrapper<>(loginlog));
        return success(getDataTable(list));
    }

    /**
     * 用户登录日志逻辑删除
     *
     * @param ids
     * @return
     */
    @ApiOperation(value = "用户登录日志逻辑删除", response = JsonResult.class)
    @ApiImplicitParam(name = "ids", value = "主键", required = true, paramType = "query")
    @RequiresPermissions("api:loginlog:remove")
    @PostMapping("/remove")
    @LoginRequired
    public JsonResult remove(String ids) {
        List<Integer> idsList = Arrays.asList(Convert.toIntArray(ids));
        if (loginlogService.removeByIds(idsList)) {
            return success();
        } else {
            return error();
        }
    }
}
