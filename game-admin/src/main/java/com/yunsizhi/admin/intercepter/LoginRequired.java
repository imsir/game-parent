package com.yunsizhi.admin.intercepter;

import java.lang.annotation.*;

/**
 * 登录拦截
*
 * @author ZJL-029
 */
@Documented
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginRequired {

}
