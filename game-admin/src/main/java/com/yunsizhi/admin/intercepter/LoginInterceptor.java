package com.yunsizhi.admin.intercepter;

import com.alibaba.fastjson.JSONObject;
import com.yunsizhi.auth.shiro.service.TokenService;
import com.yunsizhi.common.enums.GlobalCodeEnum;
import com.yunsizhi.common.result.JsonResult;
import com.yunsizhi.common.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;

/**
 * 登录验证拦截
 *
 * @author ZJL-029
 * @date 2018年5月30日 下午8:45:38
 */
@Slf4j
@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private TokenService tokenService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            LoginRequired loginRequired = findAnnotation((HandlerMethod) handler, LoginRequired.class);
            // 没有声明需要权限,或者声明不验证权限
            if (loginRequired == null) {
                return true;
            } else {
                String token = request.getHeader("token");
                log.info("requestUrl={},token={}", request.getRequestURI(), token);
                /**
                 * 在这里实现自己的权限验证逻辑
                 * 如果验证成功返回true（这里直接写false来模拟验证失败的处理）
                 */
                if (StringUtils.isEmpty(token)) {
                    response.setContentType("application/json;charset=utf-8");
                    response.getWriter().write(JSONObject.toJSONString(JsonResult.error(GlobalCodeEnum.NOT_LOGIN)));
                    System.out.println("您还未登录");
                    return false;
                } else if (!tokenService.checkToken(token)) {
                    response.setContentType("application/json;charset=utf-8");
                    response.getWriter().write(JSONObject.toJSONString(JsonResult.error(GlobalCodeEnum.TOKEN_LOSE_EFFICACY)));
                    System.out.println("登陆已失效，请退出重新登录");
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return true;
        }
    }

    private <T extends Annotation> T findAnnotation(HandlerMethod handler, Class<T> annotationType) {
        T annotation = handler.getBeanType().getAnnotation(annotationType);
        if (annotation != null) {
            return annotation;
        }
        return handler.getMethodAnnotation(annotationType);
    }



}
