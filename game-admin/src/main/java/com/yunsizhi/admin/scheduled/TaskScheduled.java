package com.yunsizhi.admin.scheduled;

import com.yunsizhi.common.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 公共定时器
 *
 * @author ZJL-029
 * @Date: 2020/8/31 14:08
 **/
@Component
@Slf4j
public class TaskScheduled {

    @Autowired
    RedisUtil redisUtil;

    /**
     * 定时任务:测试定时任务
     */
    //@Scheduled(cron = "0 0/1 * * * ?")
    public void testTask() {
        log.info("------------ 执行 testTask 任务 ------------");
        log.info("一顿操作");
        log.info("------------ testTask 任务结束 ------------");

    }

}
