package com.yunsizhi.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 启动程序
 *
 * @author zjl
 */
@SpringBootApplication
@ComponentScan("com.yunsizhi")
@EnableMongoRepositories("com.yunsizhi.repository")
@EnableSwagger2
@EnableScheduling
public class GameAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(GameAdminApplication.class, args);
        System.out.println("  _____ _______       _____ _______");
        System.out.println(" / ____|__   __|/\\   |  __ \\__   __|");
        System.out.println(" |(___    | |  /  \\  | |__) | | |   ");
        System.out.println(" \\___ \\   | | / /\\ \\ |  _  /  | |   ");
        System.out.println(" ____) |  | |/ ____ \\| | \\ \\  | |   ");
        System.out.println(" |_____/  |_/_/    \\_\\_|  \\_\\ |_|   ");
    }
}
