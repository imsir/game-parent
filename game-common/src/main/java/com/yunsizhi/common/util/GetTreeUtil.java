package com.yunsizhi.common.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 反射获取树结构数据
 */
public class GetTreeUtil {

    /**
     * 获取树结构
     *
     * @param list
     * @param maxPid
     * @param getIdMethod
     * @param getPidMethod
     * @param setChildrenMethod
     * @return
     */
    public static List<?> getTreeList(List<?> list, Long maxPid, String getIdMethod, String getPidMethod, String setChildrenMethod) {
        try {
            Map<Object, Object> dtoMap = new HashMap<Object, Object>();
            for (Object obj : list) {
                Method getId = obj.getClass().getMethod(getIdMethod);
                Object id = getId.invoke(obj);
                dtoMap.put(id, obj);
            }

            List<Object> resultList = new ArrayList<Object>();

            for (Map.Entry<Object, Object> entry : dtoMap.entrySet()) {
                Object node = entry.getValue();
                Method getPid = node.getClass().getDeclaredMethod(getPidMethod);
                Object pid = getPid.invoke(node);
                if (maxPid.equals((Long) pid)) {
                    // 如果是顶层节点，直接添加到结果集合中
                    resultList.add(node);
                } else {
                    //判断是否存在上级科目
                    if (dtoMap.get(pid) != null) {
                        Method setChildrenList = dtoMap.get(pid).getClass().getDeclaredMethod(setChildrenMethod, node.getClass());
                        setChildrenList.invoke(dtoMap.get(pid), node);
                    } else {
                        resultList.add(node);
                    }
                }
            }
            return resultList;
        } catch (Exception e) {
            return null;
        }

    }
}

