package com.yunsizhi.common.util;

import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @description: SortUtil
 * @date: 2020/4/22 16:39
 * @author: zhouhai
 * @version: 1.0
 */
public class SortUtil {


    public static final String DESC = "desc";
    public static final String ASC = "asc";


    /**
     *功能描述 用于对 int、Integer、double、Double、long、Long、float、Float、String的list排序
     * @author zhouhai
     * @date 2020/4/26
     * @param list 需要排序的列表
     * @param sort  排序方式 SortList.DESC(降序) SortList.ASC(升序)
     * @return java.util.List<?>
     */
    public static List<?> basicSort(List<?> list,String sort){
        if (CollectionUtils.isEmpty(list)) {
            return list;
        }
        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object a, Object b) {
                int ret = 0;
                try {
                    Class<?> type = a.getClass();

                    if (type == int.class || type == Integer.class) {
                        ret = ((Integer) a).compareTo((Integer) b);
                    } else if (type == double.class || type == Double.class) {
                        ret = ((Double) a).compareTo((Double) b);
                    } else if (type == long.class || type == Long.class) {
                        ret = ((Long) a).compareTo((Long) b);
                    } else if (type == float.class || type == Float.class) {
                        ret = ((Float)a).compareTo((Float) b);
                    }  else {
                        ret = String.valueOf(a).compareTo(String.valueOf(b));
                    }

                } catch (SecurityException e) {
                    e.printStackTrace();
                }
                if (sort != null && sort.toLowerCase().equals(DESC)) {
                    return -ret;
                } else {
                    return ret;
                }

            }
        });
        return list;
    }


    /**
     * 功能描述 对list中的元素按升序排列
     *
     * @param list  排序集合
     * @param field 排序字段
     * @return java.util.List<?>
     * @author zhouhai
     * @date 2020/4/26
     */
    public static List<?> sort(List<?> list, final String field) {
        return sort(list, field, null);
    }

    /**
     * 功能描述 对list中的元素根据单个字段进行排序
     * @param list  排序集合
     * @param field 排序字段
     * @param sort  排序方式: SortList.DESC(降序) SortList.ASC(升序).
     * @return java.util.List<?>
     * @author zhouhai
     * @date 2020/4/26
     */
    public static List<?> sort(List<?> list, final String field,
                               final String sort) {
        if (CollectionUtils.isEmpty(list)) {
            return list;
        }
        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object a, Object b) {
                int ret = 0;
                try {
                    Field f = a.getClass().getDeclaredField(field);
                    f.setAccessible(true);
                    Class<?> type = f.getType();

                    if (type == int.class) {
                        ret = ((Integer) f.getInt(a)).compareTo((Integer) f
                                .getInt(b));
                    } else if (type == double.class) {
                        ret = ((Double) f.getDouble(a)).compareTo((Double) f
                                .getDouble(b));
                    } else if (type == long.class) {
                        ret = ((Long) f.getLong(a)).compareTo((Long) f
                                .getLong(b));
                    } else if (type == float.class) {
                        ret = ((Float) f.getFloat(a)).compareTo((Float) f
                                .getFloat(b));
                    } else if (type == Date.class) {
                        ret = ((Date) f.get(a)).compareTo((Date) f.get(b));
                    } else if (isImplementsOf(type, Comparable.class)) {
                        ret = ((Comparable) f.get(a)).compareTo((Comparable) f
                                .get(b));
                    } else {
                        ret = String.valueOf(f.get(a)).compareTo(
                                String.valueOf(f.get(b)));
                    }

                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (sort != null && sort.toLowerCase().equals(DESC)) {
                    return -ret;
                } else {
                    return ret;
                }

            }
        });
        return list;
    }

    /**
     *功能描述   对list中的元素按fields和sorts进行排序,
     *          fields[i]指定排序字段,sorts[i]指定排序方式.如果sorts[i]为空则默认按升序排列.
     * @author zhouhai
     * @date 2020/4/26
     * @param list 需要排序的列表
     * @param fields   排序字段集合
     * @param sorts     排序方式
     * @return java.util.List<?>
     */
    public static List<?> sort(List<?> list, String[] fields, String[] sorts) {
        if (fields != null && fields.length > 0) {
            for (int i = fields.length - 1; i >= 0; i--) {
                final String field = fields[i];
                String tmpSort = ASC;
                if (sorts != null && sorts.length > i && sorts[i] != null) {
                    tmpSort = sorts[i];
                }
                final String sort = tmpSort;
                Collections.sort(list, new Comparator() {
                    @Override
                    public int compare(Object a, Object b) {
                        int ret = 0;
                        try {
                            Field f = a.getClass().getDeclaredField(field);
                            f.setAccessible(true);
                            Class<?> type = f.getType();
                            if (type == int.class) {
                                ret = ((Integer) f.getInt(a))
                                        .compareTo((Integer) f.getInt(b));
                            } else if (type == double.class) {
                                ret = ((Double) f.getDouble(a))
                                        .compareTo((Double) f.getDouble(b));
                            } else if (type == long.class) {
                                ret = ((Long) f.getLong(a)).compareTo((Long) f
                                        .getLong(b));
                            } else if (type == float.class) {
                                ret = ((Float) f.getFloat(a))
                                        .compareTo((Float) f.getFloat(b));
                            } else if (type == Date.class) {
                                ret = ((Date) f.get(a)).compareTo((Date) f
                                        .get(b));
                            } else if (isImplementsOf(type, Comparable.class)) {
                                ret = ((Comparable) f.get(a))
                                        .compareTo((Comparable) f.get(b));
                            } else {
                                ret = String.valueOf(f.get(a)).compareTo(
                                        String.valueOf(f.get(b)));
                            }

                        } catch (SecurityException e) {
                            e.printStackTrace();
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }

                        if (sort != null && sort.toLowerCase().equals(DESC)) {
                            return -ret;
                        } else {
                            return ret;
                        }
                    }
                });
            }
        }
        return list;
    }


    /**
     *功能描述 判断对象实现的所有接口中是否包含szInterface
     * @author zhouhai
     * @date 2020/4/26
     * @param clazz
     * @param szInterface
     * @return boolean
     */
    public static boolean isImplementsOf(Class<?> clazz, Class<?> szInterface) {
        boolean flag = false;
        Class<?>[] face = clazz.getInterfaces();
        for (Class<?> c : face) {
            if (c == szInterface) {
                flag = true;
            } else {
                flag = isImplementsOf(c, szInterface);
            }
        }
        if (!flag && null != clazz.getSuperclass()) {
            return isImplementsOf(clazz.getSuperclass(), szInterface);
        }
        return flag;
    }

    /**
     * 功能描述 List 排序demo
     *
     * @param args
     * @return void
     * @author zhouhai
     * @date 2020/4/22
     */
    public static void main(String[] args) {
        List<Long> list = new ArrayList<>();
        list.add(5L);
        list.add(9L);
        list.add(7L);
        list.add(2L);
        basicSort(list,DESC);
        System.out.println(list);
    }
}
