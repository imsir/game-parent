package com.yunsizhi.common.util;

/**
 * Copyright (c) 2020. yunsizhi All Rights Reserved. It is strictly forbidden to leak and use it for other commercial purposes
 * </p>
 * 长整型工具
 * </p>
 * projectName：云思智游戏
 *
 * @author CharlesLee
 * @version 1
 * @date 2020/9/17 10:55
 * @since 1.8
 */
public final class LongUtil {

    private static final Sequence SEQUENCE = new Sequence();

    private LongUtil() {
    }

    /**
     * 根据uuid生成id,无规则可言
     */
    public static long getId() {
        return SEQUENCE.nextId();
    }
}
