package com.yunsizhi.common.util;

import java.math.BigDecimal;
import java.util.Random;

/**
 * @description: NumberUtil
 * @date: 2020/4/7 13:43
 * @author: zhouhai
 * @version: 1.0
 */
public class NumberUtil {

    /**
     *功能描述 获取指定长度随机数
     * @author zhouhai
     * @date 2020/4/7
     * @param size 随机数长度
     * @return java.lang.String
     */
    public static String getRandomNumber(int size){
        if (size<=0){
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder("");
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }

    /**
     *功能描述 加法 num1+num2
     * @author zhouhai
     * @date 2020/4/7
     * @param num1
     * @param num2
     * @return java.math.BigDecimal
     */
    public static BigDecimal add(BigDecimal num1,BigDecimal num2){
        return num1.add(num2);
    }

    /**
     *功能描述 减法 num1-num2
     * @author zhouhai
     * @date 2020/4/7
     * @param num1
     * @param num2
     * @return java.math.BigDecimal
     */
    public static BigDecimal subtract(BigDecimal num1,BigDecimal num2){
        return num1.subtract(num2);
    }

    /**
     *功能描述 乘法 num1*num2
     * @author zhouhai
     * @date 2020/4/7
     * @param num1
     * @param num2
     * @return java.math.BigDecimal
     */
    public static BigDecimal multiply(BigDecimal num1,BigDecimal num2){
        return num1.multiply(num2);
    }

    /**
     *功能描述 除法 num1/num2
     * @author zhouhai
     * @date 2020/4/7
     * @param num1
     * @param num2
     * @param scale 小数位数
     * @return java.math.BigDecimal
     */
    public static BigDecimal divide(BigDecimal num1,BigDecimal num2,int scale){
        return num1.divide(num2,scale,BigDecimal.ROUND_HALF_UP);
    }

}
