package com.yunsizhi.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Copyright (c) 2020. yunsizhi All Rights Reserved. It is strictly forbidden to leak and use it for other commercial purposes
 * </p>
 * 分布式高效有序ID生产黑科技(sequence)
 * </p>
 * projectName：云思智游戏
 *
 * @author CharlesLee
 * @version 1
 * @date 7/16/2020 5:27 AM
 * @since 1
 */
public final class Sequence {

    private static final Logger LOGGER = LoggerFactory.getLogger(Sequence.class);

    /**
     * 时间起始标记点，作为基准，一般取系统的最近时间（一旦确定不能变动）
     */
    private final long twepoch = System.currentTimeMillis();
    /**
     * 机器标识位数
     */
    private final long workerIdBits = 5L;
    private final long dateCenterIdBits = 5L;

    private final long workerId;

    /**
     * 数据标识 ID 部分
     */
    private final long datacenterId;

    /**
     * 并发控制
     */
    private long sequence = 0L;

    /**
     * 上次生产 ID 时间戳
     */
    private long lastTimestamp = -1L;


    public Sequence() {
        long maxDateCenterId = ~(-1L << dateCenterIdBits);
        this.datacenterId = getDatacenterId(maxDateCenterId);
        long maxWorkerId = ~(-1L << workerIdBits);
        this.workerId = getMaxWorkerId(datacenterId, maxWorkerId);
    }

    /**
     * 获取 maxWorkerId
     */
    protected static long getMaxWorkerId(long datacenterId, long maxWorkerId) {
        StringBuilder mpid = new StringBuilder();
        mpid.append(datacenterId);
        String name = ManagementFactory.getRuntimeMXBean().getName();
        if (StringUtils.isNotEmpty(name)) {
            /*
             * GET jvmPid
             */
            mpid.append(name.split("@")[0]);
        }
        /*
         * MAC + PID 的 hashcode 获取16个低位
         */
        return (mpid.toString().hashCode() & 0xffff) % (maxWorkerId + 1);
    }


    /**
     * 数据标识id部分
     */
    private static long getDatacenterId(long maxDateCenterId) {
        long id = 0L;
        try {
            InetAddress ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            if (network == null) {
                id = 1L;
            } else {
                byte[] mac = network.getHardwareAddress();
                if (null != mac) {
                    id = ((0x000000FF & (long) mac[mac.length - 1]) | (0x0000FF00 & (((long) mac[mac.length - 2]) << 8))) >> 6;
                    id = id % (maxDateCenterId + 1);
                }
            }
        } catch (Exception e) {
            LOGGER.error(" getDatacenterId: ", e);
        }
        return id;
    }


    /**
     * 获取下一个 ID
     *
     * @return 下一个 ID
     */
    public synchronized long nextId() {
        long timestamp = System.currentTimeMillis();
        //闰秒
        if (timestamp < lastTimestamp) {
            long offset = lastTimestamp - timestamp;
            if (offset <= 5) {
                try {
                    wait(offset << 1);
                    timestamp = System.currentTimeMillis();
                    if (timestamp < lastTimestamp) {
                        throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", offset));
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            } else {
                throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", offset));
            }
        }

        // 毫秒内自增位
        long sequenceBits = 12L;
        if (lastTimestamp == timestamp) {
            // 相同毫秒内，序列号自增
            long sequenceMask = ~(-1L << sequenceBits);
            sequence = (sequence + 1) & sequenceMask;
            if (sequence == 0) {
                // 同一毫秒的序列数已经达到最大
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            // 不同毫秒内，序列号置为 1 - 3 随机数
            sequence = ThreadLocalRandom.current().nextLong(1, 3);
        }

        lastTimestamp = timestamp;

        // 时间戳部分 | 数据中心部分 | 机器标识部分 | 序列号部分
        // 时间戳左移动位
        long timestampLeftShift = sequenceBits + workerIdBits + dateCenterIdBits;
        long dateCenterIdShift = sequenceBits + workerIdBits;
        return ((timestamp - twepoch) << timestampLeftShift)
                | (datacenterId << dateCenterIdShift)
                | (workerId << sequenceBits)
                | sequence;
    }

    private long tilNextMillis(long lastTimestamp) {
        long timestamp = System.currentTimeMillis();
        while (timestamp <= lastTimestamp) {
            timestamp = System.currentTimeMillis();
        }
        return timestamp;
    }
}
