package com.yunsizhi.common.result;

import com.yunsizhi.common.enums.GlobalCodeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * 操作消息提醒
 *
 * @author zjl
 */
public class JsonResult extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    /**
     * 初始化一个新创建的 Message 对象
     */
    public JsonResult() {
    }

    /**
     * 返回错误消息
     *
     * @return 错误消息
     */
    public static JsonResult error() {
        return error(GlobalCodeEnum.FAIL.getCode(), "操作失败");
    }

    public static JsonResult error(Map<String, String> map) {
        return error(GlobalCodeEnum.FAIL.getCode(), map.get("msg"));
    }

    public static JsonResult error(GlobalCodeEnum globalCodeEnum) {
        return error(globalCodeEnum.getCode(),globalCodeEnum.getMessage());
    }

    /**
     * 返回错误消息
     *
     * @param msg 内容
     * @return 错误消息
     */
    public static JsonResult error(String msg) {
        return error(GlobalCodeEnum.FAIL.getCode(), msg);
    }

    /**
     * 返回错误消息
     *
     * @param code 错误码
     * @param msg  内容
     * @return 错误消息
     */
    public static JsonResult error(Integer code, String msg) {
        JsonResult json = new JsonResult();
        json.put("code", code);
        json.put("msg", msg);
        return json;
    }

    /**
     * 返回成功消息
     *
     * @param msg 内容
     * @return 成功消息
     */
    public static JsonResult success(String msg) {
        JsonResult json = new JsonResult();
        json.put("msg", msg);
        json.put("code", GlobalCodeEnum.SUCCESS.getCode());
        return json;
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static JsonResult success() {
        return JsonResult.success("操作成功");
    }

    public static JsonResult success(Object object) {
        JsonResult json = new JsonResult();
        json.put("msg", "操作成功");
        json.put("code", GlobalCodeEnum.SUCCESS.getCode());
        json.put("data", object);
        return json;
    }

    /**
     * 返回成功消息
     *
     * @param key   键值
     * @param value 内容
     * @return 成功消息
     */
    @Override
    public JsonResult put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
