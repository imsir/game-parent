package com.yunsizhi.common.constants;

public class SystemConstants {


    /**
     * 系统消息key
     *
     * @author kimi
     * @dateTime 2012-3-9 下午3:09:13
     */
    public static final String MESSAGE = "message";

    /**
     * 成功操作标识
     *
     * @author kimi
     * @dateTime 2012-3-9 下午3:09:15
     */
    public static final String SUCCESS = "OK";

    /**
     * 警告操作标识
     *
     * @author kimi
     * @dateTime 2012-3-9 下午3:09:17
     */
    public static final String WARN = "warn";

    /**
     * 错误标识
     *
     * @author kimi
     * @dateTime 2012-3-9 下午3:09:18
     */
    public static final String ERROR = "error";

    /**
     * 启用状态标识
     *
     * @author kimi
     * @dateTime 2012-3-9 下午3:09:26
     */
    public static final Integer STATUS_OK = Integer.parseInt("0");

    /**
     * 禁用状态标识
     *
     * @author kimi
     * @dateTime 2012-3-9 下午3:09:30
     */
    public static final Integer STATUS_NO = Integer.parseInt("1");
    /**
     * 云存储上传目录
     */
    public static final String UPLOAD_DIR = "/photos/";
    /**
     * 云存储资源访问目录
     */
    public static final String VISIT_DIR = "http://cdn.scncry.cn/photos/";


    /**
     * 纠错练习答案记录key
     */
    public static final String ERROR_TIME = "ERROR_TIME_";

    /**
     * 限时练习答案记录key
     */
    public static final String LIMIT_PREFIX = "LIMIT_";
    /**
     * 限时练习计时key
     */
    public static final String LIMIT_TIME = "LIMIT_TIMIE_";
    /**
     * 开始答题标识符
     */
    public static final String LIMIT_STR = "LIMIT_STR";
    /**
     * 开始答题标识符--已开始
     */
    public static final String LIMIT_STR_OK = "OK";

    /**
     * 微课标识符
     */
    public static final String ERROR_MIN_CLASS = "ERROR_MIN_CLASS_SUBMIT";


}
