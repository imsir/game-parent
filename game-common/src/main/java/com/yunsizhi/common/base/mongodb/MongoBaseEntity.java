package com.yunsizhi.common.base.mongodb;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体继承类
 *
 * @author zjl
 * @date 2019/4/13
 */
@Data
public class MongoBaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @Id
    private Long id;

    @ApiModelProperty(value = "创建人", hidden = true)
    @Field("creator")
    private String creator;

    @ApiModelProperty(value = "状态", hidden = true)
    @Field("status")
    private Integer status;

    @ApiModelProperty(value = "是否删除", hidden = true)
    @Field("deleted")
    private Boolean deleted;

    @ApiModelProperty(value = "是否禁用", hidden = true)
    @Field("deleted")
    private Boolean disable;


    @ApiModelProperty(value = "创建人id", hidden = true)
    @Field("creator_id")
    private Long creatorId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "创建时间", hidden = true)
    @Field("creator_time")
    private Date creatorTime;

    @ApiModelProperty(value = "最近操作人", hidden = true)
    @Field("last_operator")
    private String lastOperator;

    @ApiModelProperty(value = "最近操作人id", hidden = true)
    @Field("last_operator_id")
    private Long lastOperatorId;

    @ApiModelProperty(value = "版本", hidden = true)
    @Field("version")
    private Long version;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "修改时间", hidden = true)
    @Field("last_operator_time")
    private Date lastOperatorTime;

    @ApiModelProperty(value = "备注", hidden = true)
    @Field("remark")
    private String remark;

    public static final String ID = "id";

    public static final String CREATOR = "creator";

    public static final String CREATOR_ID = "creator_id";

    public static final String CREATOR_TIME = "creator_time";

    public static final String LAST_OPERATOR = "last_operator";

    public static final String LAST_OPERATOR_ID = "last_operator_id";

    public static final String LAST_OPERATOR_TIME = "last_operator_time";


    /**
     * Is new boolean.
     *
     * @return the boolean
     */

    @JsonIgnore
    public boolean isNew() {
        return this.id == null;
    }

    @JsonIgnore
    public void addProperties() {
        if (isNew()) {
            this.creatorTime = new Date();
        }
        this.lastOperatorTime = new Date();
    }

}
