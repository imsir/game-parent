package com.yunsizhi.common.base.page;

import lombok.Data;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Zhongjunlin
 * @date 2020/10/10 13:15
 */
@Data
public class PageHelper<T> {

    private Integer pageNum;
    private Integer total;
    private Integer pageSize;
    private List<T> list;

    public PageHelper(Integer pageNum, Integer pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public void PageHelper(Integer pageNum, Integer pageSize, Query query) {
        pageSize = pageSize == 0 ? 10 : pageSize;
        query.limit(pageSize);
        query.skip((pageNum - 1) * pageSize);
        this.pageSize = pageSize;
        this.pageNum = pageNum;
    }

    public void PageHelper(Integer total, List<T> list) {
        this.total = total;
        this.list = list;
    }

    public PageHelper(Integer pageNum, Integer total, Integer pageSize, List<T> list) {
        this.pageNum = pageNum;
        this.total = total;
        this.pageSize = pageSize;
        this.list = list;
    }

    public PageHelper(Integer pageNum, Integer pageSize, List<T> list) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.list = list;
    }


    /**
     * 用于模糊查询忽略大小写
     * criteria.and("name").regex(".*?" + bean.getName() + ".*", "i");
     *
     * @param string
     * @return
     */
    public Pattern getPattern(String string) {
        Pattern pattern = Pattern.compile("^.*" + string + ".*$", Pattern.CASE_INSENSITIVE);
        return pattern;
    }

    /**
     * 转义正则特殊字符 （$()*+.[]?\^{},|）
     *
     * @param keyword
     * @return
     */
    public String escapeExprSpecialWord(String keyword) {
        String[] fbsArr = {"\\", "$", "(", ")", "*", "+", ".", "[", "]", "?", "^", "{", "}", "|"};
        for (String key : fbsArr) {
            if (keyword.contains(key)) {
                keyword = keyword.replace(key, "\\" + key);
            }
        }
        return keyword;
    }
}
