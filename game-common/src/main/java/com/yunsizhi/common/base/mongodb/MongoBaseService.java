package com.yunsizhi.common.base.mongodb;


import com.yunsizhi.common.base.page.PageHelper;

import java.util.List;

/**
 * MongoDB操作接口
 *
 * @author Zhongjunlin
 * @date 2020/09/28 10:28
 */
public interface MongoBaseService<T> {

    /**
     * 添加
     *
     * @param bean 实体对象
     */
    void add(T bean);

    /**
     * 删除
     *
     * @param id 主键
     */
    void remove(Long id);

    /**
     * 批量删除
     *
     * @param ids
     */
    void remove(List<Long> ids);

    /**
     * 逻辑删除
     *
     * @param id
     */
    void logicalRemove(Long id);

    /**
     * 逻辑删除
     *
     * @param ids
     */
    void logicalRemove(List<Long> ids);

    /**
     * 修改
     *
     * @param bean 实体对象
     */
    void update(T bean);

    /**
     * 根据id查询
     *
     * @param id 主键
     * @return
     */
    T findById(Long id);

    /**
     * 分页查询
     *
     * @param page 分页对象
     * @param bean 查询实体对象
     * @return
     */
    PageHelper<T> findByPage(PageHelper<T> page, T bean);
}
