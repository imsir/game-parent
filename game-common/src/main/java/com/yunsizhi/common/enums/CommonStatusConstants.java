package com.yunsizhi.common.enums;

/**
 * 业务公用类
 *
 * @author zyg
 * @date 2020年7月28日 下午10:38:33
 */
public enum CommonStatusConstants {

    NOT_USED(0, "禁用"),
    USED(1, "启用");

    private String desc;

    private Integer code;

    CommonStatusConstants(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
