package com.yunsizhi.common.enums;

import lombok.Getter;

/**
 * 全局状态码
 */
@Getter
public enum GlobalCodeEnum {

    SUCCESS(200, "操作成功"),
    FAIL(500, "操作失败"),
    STATUS_OK(0, "正常"),
    STATUS_FAIL(1, "禁用"),
    DELETE_FALSE(0, "未删除"),
    DELETE_TRUE(1, "已删除"),


    USER_BLOCKED(10010, "用户已被禁用"),

    // 系统错误状态码
    GENERAL_EXCEPTION(1000, "通用异常"),

    SEND_SMS_ERROR(10001, "短信发送失败"),
    // 该用户已存在
    USER_ALREADY_EXIST(1001, "用户名已存在"),
    // 验证码无效
    CODE_EXPIRE(1002, "验证码无效"),
    // 验证码不正确
    CODE_ERROR(1003, "验证码不正确"),
    // 用户名不存在
    USERNAME_NOT_EXIST(1004, "用户名不存在"),
    // 密码不正确
    PASSWORD_ERROR(1005, "密码不正确"),
    // 没有相关权限
    NOT_AUTH(1006, "没有相关权限"),
    // token无效
    TOKEN_INVALID(1007, "token failure!"),
    // 缺少相应参数
    MISSING_PARAMETER(1008, "参数绑定失败:缺少参数"),
    // 接口请求限制
    REQUEST_LIMIT(10009, "请求频繁,请稍后重试"),



    REQUEST_ERROR(400, "请求错误"),
    UNAUTHORIZED(401, "未授权"),
    NOT_ACCESSIBLE(403, "不可访问"),
    METHOD_NOT_ALLOWED(405, "方法不被允许"),
    UNSUPPORTED_MEDIA_TYPE(415, "不支持当前媒体类型"),
    NOT_LOGIN(406, "您还未登录"),
    LOGIN_INVALID(407, "登录失效,请重新登录"),
    FORCED_OFFLINE(407, "你已被强制下线"),
    TOKEN_LOSE_EFFICACY(402,"您的登录令牌已失效，请重新登录")
    ;


    private Integer code;

    private String message;

    GlobalCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static GlobalCodeEnum getByCode(String code) {

        for (GlobalCodeEnum errorCode : GlobalCodeEnum.values()) {
            if (errorCode.getCode().equals(code)) {
                return errorCode;
            }
        }

        return null;
    }
}
