package com.yunsizhi.common.enums;

/**
 * 业务公用类
 *
 * @author chenbiao
 * @date 2018年7月28日 下午10:38:33
 */
public class ServiceConstants {

    /**
     * 用户token相关
     */
    public static final String USER_TOKEN_KEY = "game:user:token:";

    /**
     * 防盗链Token秘钥
     */
    public static final String SECRET_KEY = "6fdfMZhnI2nCGiWcbbBW8ryZqqx132Jg";

    /**
     * 视频云存储上传目录
     */
    public static final String UPLOAD_DIR = "/video/";
    /**
     * 图片云存储上传目录
     */
    public static final String UPLOAD_PHOTOS_DIR = "/photos/";


    /**
     * 云存储视频资源访问目录
     */
    public static final String VIDEO_VISIT_DIR = "http://websitecdn.yunsizhi.com/video/";

    /**
     * 云存储图片资源访问目录
     */
    public static final String PICTURE_VISIT_DIR = "http://webimg.yunsizhi.com/photos/";

}
