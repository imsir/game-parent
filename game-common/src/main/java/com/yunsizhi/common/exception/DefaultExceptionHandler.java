package com.yunsizhi.common.exception;


import com.yunsizhi.common.result.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 自定义异常处理器
 *
 * @author zjl
 */

@Slf4j
@RestControllerAdvice
public class DefaultExceptionHandler {

    /**
     * 通用业务异常
     *
     * @param e
     * @return ResponseEntity
     */
    @ExceptionHandler(BaseException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public JsonResult handleServiceException(BaseException e) {
        return JsonResult.error(400, "服务器错误");
    }

    /**
     * 参数验证异常
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public JsonResult handleConstraintViolationException(ValidationException exception) {
        if (exception instanceof ConstraintViolationException) {
            ConstraintViolationException exs = (ConstraintViolationException) exception;
            Set<ConstraintViolation<?>> violations = exs.getConstraintViolations();
            String msg = violations.stream().map(i -> i.getMessage()).collect(Collectors.joining(","));
            return JsonResult.error(400, msg);
        }
        return JsonResult.error(400, "请求参数校验失败");
    }

    /**
     * 用户已被禁用
     */
    @ExceptionHandler(UserBlockedException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public JsonResult handleUserBlockedException() {
        return JsonResult.error(401, "用户已被禁用");
    }

    /**
     * token无效异常
     */
    @ExceptionHandler(IncorrectCredentialsException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public JsonResult handleTokenException() {
        return JsonResult.error(400, "Token无效");
    }


    /**
     * 参数校验(缺少)异常处理
     *
     * @return ResponseEntity
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public JsonResult handleMissingParameterException() {
        return JsonResult.error(400, "缺少必要参数");
    }

    /**
     * 登录超时
     */
    @ExceptionHandler(UnauthenticatedException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public JsonResult handleUnauthenticatedException(UnauthenticatedException e) {
        log.error(e.getMessage(), e);
        return JsonResult.error("您没有数据的权限");
    }

    /**
     * 权限校验失败
     */
    @ExceptionHandler(AuthorizationException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public JsonResult handleAuthorizationException(AuthorizationException e) {
        log.error(e.getMessage(), e);
        return JsonResult.error("您没有数据的权限");
    }

    /**
     * 请求方式不支持
     */
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public JsonResult handleException(HttpRequestMethodNotSupportedException e) {
        log.error(e.getMessage(), e);
        return JsonResult.error("不支持' " + e.getMethod() + "'请求");
    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public JsonResult notFount(RuntimeException e) {
        log.error("运行时异常:", e);
        return JsonResult.error("运行时异常");
    }

    /**
     * 系统异常
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public JsonResult handleException(Exception e) {
        log.error(e.getMessage(), e);
        return JsonResult.error("服务器内部错误");
    }

}
