package com.yunsizhi.common.exception;

import org.apache.shiro.authc.CredentialsException;

/**
 * 用户锁定异常类
 *
 * @author zjl
 */
public class UserBlockedException extends CredentialsException {

	public UserBlockedException() {
	}

	public UserBlockedException(String message) {
		super(message);
	}

	public UserBlockedException(Throwable cause) {
		super(cause);
	}

	public UserBlockedException(String message, Throwable cause) {
		super(message, cause);
	}
}
