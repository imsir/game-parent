package com.yunsizhi.repository;

import com.yunsizhi.model.dto.MdbTest;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Zhongjunlin
 * @date 2020/09/28 10:28
 */
public interface MdbTestRepository extends MongoRepository<MdbTest, Long> {
    /**
     * .
     * 通过手机号查询
     *
     * @param tel 手机号
     * @return adminUserDO
     */
    MdbTest getByTel(String tel);

    /**
     * 通过id 获取admin用户信息
     *
     * @param id id
     * @return AdminUserDO
     */
    MdbTest getById(Long id);
}
