package com.yunsizhi.model.dto;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yunsizhi.common.base.BaseEntity;
import lombok.Data;

/**
 * 字典数据表 sys_dict_data
 *
 * @author ZJL-029
 */
@Data
@TableName("sys_dict_data")
public class DictData extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 字典排序
     */
    @TableField("dict_sort")
    private Long dictSort;

    /**
     * 字典标签
     */
    @TableField("dict_label")
    private String dictLabel;

    /**
     * 字典键值
     */
    @TableField("dict_value")
    private String dictValue;

    /**
     * 字典类型
     */
    @TableField("dict_type")
    private String dictType;

    /**
     * 字典样式
     */
    @TableField("css_class")
    private String cssClass;

    /**
     * 是否默认（Y是 N否）
     */
    @TableField("is_default")
    private String isDefault;

}
