package com.yunsizhi.model.dto;


import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统登录日志记录实体类 sys_loginlog
 *
 * @author zjl
 */
@Data
@TableName("sys_loginlog")
public class SysLoginLog extends Model<SysLoginLog> {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId("info_id")
    private Integer infoId;

    /**
     * 用户账号
     */
    @TableField("login_name")
    private String loginName;

    /**
     * 登录状态 0成功 1失败
     */
    private String status;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 登录地点
     */
    @TableField("login_location")
    private String loginLocation;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 提示消息
     */
    private String msg;

    /**
     * 访问时间
     */
    @TableField("login_time")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date loginTime;

    /**
     * 是否删除 0：未删除1：删除
     */
    @TableField("is_del")
    private Integer isDel;

    @Override
    protected Serializable pkVal() {
        return this.infoId;
    }

}
