package com.yunsizhi.model.dto;


import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 操作日志记录 sys_oper_log
 *
 * @author ZJL-029
 */
@Data
@TableName("sys_oper_log")
public class SysOperLog extends Model<SysOperLog> {
	private static final long serialVersionUID = 1L;

	/** 日志主键 */
	@TableId("oper_id")
	private Integer operId;

	/** 操作模块 */
	private String title;

	/** 操作类型 */
	private String action;

	/** 请求方法 */
	private String method;

	/** 来源渠道 */
	private String channel;

	/** 操作人员 */
	@TableField("oper_name")
	private String operName;

	/** 请求url */
	@TableField("oper_url")
	private String operUrl;

	/** 操作地址 */
	@TableField("oper_ip")
	private String operIp;

	/** 操作地点 */
	@TableField("oper_location")
	private String operLocation;

	/** 请求参数 */
	@TableField("oper_param")
	private String operParam;

	/** 状态0正常 1异常 */
	private Integer status;

	/** 错误消息 */
	@TableField("error_msg")
	private String errorMsg;

	/** 操作时间 */
	@TableField("oper_time")
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date operTime;

	/** 是否删除 0：未删除1：删除 */
	@TableField("is_del")
	private Integer isDel;

	@Override
	protected Serializable pkVal() {
		return this.operId;
	}

}
