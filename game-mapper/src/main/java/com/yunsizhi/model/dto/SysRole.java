package com.yunsizhi.model.dto;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yunsizhi.common.base.BaseEntity;
import lombok.Data;

/**
 * 角色对象 sys_role
 *
 * @author ZJL-029
 */
@Data
@TableName("sys_role")
public class SysRole extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /**
     * 角色名称
     */
    @TableField("role_name")
    private String roleName;

    /**
     * 角色权限
     */
    @TableField("role_key")
    private String roleKey;

    /**
     * 角色排序
     */
    @TableField("role_sort")
    private String roleSort;

    /**
     * 用户是否存在此角色标识 默认不存在
     */
    @TableField(exist = false)
    private boolean flag = false;

    /**
     * 菜单组
     */
    @TableField(exist = false)
    private Long[] menuIds;

}
