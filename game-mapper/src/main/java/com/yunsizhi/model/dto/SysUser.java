package com.yunsizhi.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yunsizhi.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 用户对象 sys_user
 *
 * @author ZJL-029
 */
@Data
@TableName("sys_user")
public class SysUser extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /**
     * 登录名称
     */
    @TableField("login_name")
    private String loginName;

    /**
     * 用户名称
     */
    @TableField("user_name")
    private String userName;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户性别（0男 1女 2未知）
     */
    private String sex;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 密码
     */
    private String password;

    /**
     * 盐加密
     */
    private String salt;

    /**
     * 最后登陆IP
     */
    @TableField("login_ip")
    private String loginIp;

    /**
     * 最后登陆时间
     */
    @TableField("login_date")
    private Date loginDate;

    /**
     * 角色ID
     */
    @TableField(exist = false)
    private Long roleId;

    /**
     * 角色组
     */
    @TableField(exist = false)
    private Long[] roleIds;


    /**
     * 用户是否存在此角色标识 默认不存在
     */
    @TableField(exist = false)
    private boolean flag = false;


    public static boolean isAdmin(Long userId) {
        return userId != null && 1L == userId;
    }

}
