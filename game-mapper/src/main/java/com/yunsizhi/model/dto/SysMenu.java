package com.yunsizhi.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yunsizhi.common.base.BaseEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色对象 sys_menu
 *
 * @author ZJL-029
 */
@Data
@TableName("sys_menu")
public class SysMenu extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /**
     * 菜单名称
     */
    @TableField("menu_name")
    private String menuName;

    /**
     * 父菜单名称
     */
    @TableField(exist = false)
    private String parentName;

    /**
     * 父菜单ID
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 显示顺序
     */
    @TableField("order_num")
    private String orderNum;

    /**
     * 菜单URL
     */
    private String url;

    /**
     * 类型:0目录,1菜单,2按钮
     */
    @TableField("menu_type")
    private String menuType;

    /**
     * 菜单状态:0显示,1隐藏
     */
    private int visible;

    /**
     * 权限字符串
     */
    private String perms;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 子菜单
     */
    @TableField(exist = false)
    private List<SysMenu> children = new ArrayList<SysMenu>();


}
