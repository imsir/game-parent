package com.yunsizhi.model.dto;

import com.yunsizhi.common.base.mongodb.MongoBaseEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Copyright (c) 2020. yunsizhi All Rights Reserved. It is strictly forbidden to leak and use it for other commercial purposes
 * <p>
 * 物品图鉴(物品配置实体)
 * </p>
 * projectName：云思智游戏
 *
 * @author CharlesLee
 * @version 1
 * @date 2020/10/9 10:31
 * @since 1.8
 */
@Data
@Document(collection = "mongo_demo")
public class MongoDemoDTO extends MongoBaseEntity {

    private static final long serialVersionUID = 9207963595335641041L;

    /**
     * 唯一ID
     */
    @Field("id")
    private Long id;

    /**
     * 名字
     */
    @Field("name")
    private String name;

    /**
     * 物品配置图片地址, 可以以超链接形式直接访问
     */
    @Field("img")
    private String img;
}
