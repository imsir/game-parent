package com.yunsizhi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Zhongjunlin
 * @date 2020/09/28 10:26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "test")
public class MdbTest {

    @Id
    private Long id;
    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * /**
     * 用户手机号
     */
    private String tel;

}

