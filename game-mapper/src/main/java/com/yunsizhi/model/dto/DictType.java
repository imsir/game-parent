package com.yunsizhi.model.dto;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yunsizhi.common.base.BaseEntity;
import lombok.Data;

/**
 * 字典类型对象 sys_dict_type
 * @author ZJL-029
 */
@Data
@TableName("sys_dict_type")
public class DictType extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 字典名称
     */
    @TableField("dict_name")
    private String dictName;

    /**
     * 字典类型
     */
    @TableField("dict_type")
    private String dictType;


}
