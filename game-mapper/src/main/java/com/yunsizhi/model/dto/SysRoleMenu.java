package com.yunsizhi.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色和菜单关联 sys_role_menu
 *
 * @author ZJL-029
 */
@Data
@TableName("sys_role_menu")
public class SysRoleMenu extends Model<SysRoleMenu> {
	private static final long serialVersionUID = 1L;

	/** 角色ID */
	@TableField("role_id")
	private Long roleId;

	/** 菜单ID */
	@TableField("menu_id")
	private Long menuId;


	@Override
	protected Serializable pkVal() {
		return null;
	}

}
