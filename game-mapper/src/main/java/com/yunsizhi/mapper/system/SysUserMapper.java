package com.yunsizhi.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunsizhi.model.dto.SysUser;

import java.util.List;

/**
 * 用户表 数据层
 *
 * @author ZJL-029
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> selectUserList(SysUser user);

    SysUser selectDeptNameByUserId(SysUser userId);

    List<SysUser> selectchildUserList(SysUser user);
}
