package com.yunsizhi.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunsizhi.model.dto.SysUserRole;

import java.util.List;

/**
 * 用户表 数据层
 *
 * @author ZJL-029
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    /**
     * 批量新增用户角色信息
     *
     * @param userRoleList 用户角色列表
     * @return 结果
     */
    int batchUserRole(List<SysUserRole> userRoleList);
}
