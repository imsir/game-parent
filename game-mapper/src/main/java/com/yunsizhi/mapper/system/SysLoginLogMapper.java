package com.yunsizhi.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunsizhi.model.dto.SysLoginLog;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author zjl
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {

}
