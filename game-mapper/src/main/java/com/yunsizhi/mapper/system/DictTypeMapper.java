package com.yunsizhi.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunsizhi.model.dto.DictType;

/**
 * 字典表 数据层
 *
 * @author ZJL-029
 */
public interface DictTypeMapper extends BaseMapper<DictType> {
}
