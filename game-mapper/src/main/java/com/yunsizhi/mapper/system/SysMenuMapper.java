package com.yunsizhi.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunsizhi.model.dto.SysMenu;

import java.util.List;

/**
 * 菜单表 数据层
 *
 * @author ZJL-029
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    List<String> selectPermsByUserId(Long userId);

    /**
     * 根据用户ID查询菜单MC
     *
     * @param userId
     * @return
     */
    List<SysMenu> selectMenusByUserId(Long userId);


    /**
     * 根据用户ID查询所有菜单MCF
     *
     * @param userId
     * @return
     */
    List<SysMenu> selectMenusAllByUserId(Long userId);

    /**
     * 根据角色ID查询菜单
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
    List<String> selectMenuTree(Long roleId);


    /**
     * 根据角色ID查询菜单列表
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
    List<SysMenu> selectMenuListByRole(Long roleId);

}
