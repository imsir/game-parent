package com.yunsizhi.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunsizhi.model.dto.SysRoleMenu;

import java.util.List;

/**
 * 角色与菜单关联表 数据层
 *
 * @author ZJL-029
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

	/**
     * 批量新增角色菜单信息
     *
     * @param roleMenuList 角色菜单列表
     * @return 结果
     */
    int batchRoleMenu(List<SysRoleMenu> roleMenuList);
}
