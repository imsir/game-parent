package com.yunsizhi.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunsizhi.model.dto.SysRole;

import java.util.List;
import java.util.Map;

/**
 * 角色表 数据层
 *
 * @author ZJL-029
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    List<SysRole> selectRolesByUserId(Long userId);

    List<SysRole> searchList(Map<String, Object> columnMap);
}
