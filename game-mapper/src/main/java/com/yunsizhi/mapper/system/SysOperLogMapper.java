package com.yunsizhi.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunsizhi.model.dto.SysOperLog;

/**
 * 操作日志 数据层
 *
 * @author zjl
 */
public interface SysOperLogMapper extends BaseMapper<SysOperLog> {

}
