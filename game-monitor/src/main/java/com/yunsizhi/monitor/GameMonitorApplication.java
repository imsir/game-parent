package com.yunsizhi.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * Springboot监控
 * @author ZJL-029
 */
@EnableAdminServer
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class GameMonitorApplication {
    public static void main(String[] args) {
        SpringApplication.run(GameMonitorApplication.class, args);
    }
}
