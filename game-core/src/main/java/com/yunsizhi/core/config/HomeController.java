
package com.yunsizhi.core.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * API接口
 * @author ZJL-029
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/doc")
    public String index() {
        return "redirect:swagger-ui.html";
    }
}
