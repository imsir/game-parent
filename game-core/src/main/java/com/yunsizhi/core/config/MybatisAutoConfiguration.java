package com.yunsizhi.core.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * mybatis自动配置
 * @author ZJL-029
 * @since 2.2.0
 */
@ConditionalOnClass({ DataSource.class, EmbeddedDatabaseType.class})
@ConditionalOnBean(DataSource.class)
@Import(MybatisPlusConfiguration.class)
public class MybatisAutoConfiguration {
}
