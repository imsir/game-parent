package com.yunsizhi.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * 云思智学尖生APP接口文档
 * @author lucong
 * @date 2020/04/12
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {


    @Bean
    public Docket createRestApiForPublicDirectory() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
                .groupName("后台管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yunsizhi.admin.api.controller"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(pars());
    }

    @Bean
    public Docket createRestApiForExamDirectory() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
                .groupName("游戏接口")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yunsizhi.game.api.controller"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(pars());
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("game接口文档")
                .version("1.0")
                .description("接口文档说明")
                .build();
    }

    List<Parameter> pars(){
        ParameterBuilder ticketPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<Parameter>();
        //header中的token参数非必填，传空也可以
        ticketPar.name("token").description("token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        pars.add(ticketPar.build());
        return pars;
    }

}
