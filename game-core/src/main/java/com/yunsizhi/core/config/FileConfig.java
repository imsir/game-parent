package com.yunsizhi.core.config;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.MultipartConfigElement;

/**
 * 文件描述
 *
 * @author tjm
 * @date 2018/7/27.
 */
@Configuration
public class FileConfig {
	/**
	 * 配置上传文件大小为30Mb
	 * @return
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize(1024L * 1024L * 30);
		return factory.createMultipartConfig();
	}
}
