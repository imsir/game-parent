package com.yunsizhi.core.constant;

import java.util.concurrent.TimeUnit;

/**
 * 常量类
 * @author zjl
 * @date 2019/10/3 15:45
 */
public class Constant {

    /**
     * 验证码过期时间 此处为五分钟
     */
    public static Long CODE_EXPIRE_TIME = 5L;

    /**
     * jwtToken过期时间 毫秒，当前为7天
     */
    public static Long TOKEN_EXPIRE_TIME = TimeUnit.DAYS.toMillis(7);

    /**
     * token请求头名称
     */
    public static String TOKEN_HEADER_NAME = "token";

    /**
     * 表单重复提交间隔时间 单位 S
     * 两次相同参数的请求，如果间隔时间大于该参数，系统不会认定为重复提交的数据
     */
    public static Long FORM_REPEAT_TIME = 10L;
}
