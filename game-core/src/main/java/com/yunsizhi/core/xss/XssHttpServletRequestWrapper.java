package com.yunsizhi.core.xss;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * XSS过滤处理
 *
 * @author zjl
 */
public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {

    /**
     * @param request
     */
    public XssHttpServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String[] getParameterValues(String name) {

        String[] values = super.getParameterValues(name);
        if (values != null) {

            int length = values.length;
            String[] escapseValues = new String[length];

            for (int i = 0; i < length; i++) {
                // 防xss攻击和过滤前后空格
                escapseValues[i] = values[i].trim();
                //富文本内容处理，不过虑XSS
//                if("content".equals(name)){
//                    //处理emoji表情
//                    escapseValues[i] = EmojiUtil.filterEmoji(values[i].trim(),"");
//                }else{
//                    //处理emoji表情
//                    String temp = Jsoup.clean(values[i], Whitelist.relaxed()).trim();
//                    escapseValues[i] = EmojiUtil.filterEmoji(temp,"");
//                }
            }
            return escapseValues;
        }
        return super.getParameterValues(name);
    }
}
